
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 10/30/2014 10:25:25
-- Generated from EDMX file: C:\Users\andre.steyn\documents\visual studio 2013\Projects\DDS.WebApp.PSQuoteGenerator\DDS.Core.DataLayer\DataManagementModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [DDS.WebApp.PSQuoteGenerator];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Clients]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Clients];
GO
IF OBJECT_ID(N'[dbo].[Technologies]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Technologies];
GO
IF OBJECT_ID(N'[dbo].[TechnologySteps]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TechnologySteps];
GO
IF OBJECT_ID(N'[dbo].[QuoteProcessTracks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[QuoteProcessTracks];
GO
IF OBJECT_ID(N'[dbo].[Quotes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Quotes];
GO
IF OBJECT_ID(N'[dbo].[Costs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Costs];
GO
IF OBJECT_ID(N'[dbo].[CostTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CostTypes];
GO
IF OBJECT_ID(N'[dbo].[LineItems]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LineItems];
GO
IF OBJECT_ID(N'[dbo].[VendorsPerTechnologies]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VendorsPerTechnologies];
GO
IF OBJECT_ID(N'[dbo].[AdditionalSteps]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AdditionalSteps];
GO
IF OBJECT_ID(N'[dbo].[AdditionalLineItems]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AdditionalLineItems];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Clients'
CREATE TABLE [dbo].[Clients] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL,
    [Email] nvarchar(max)  NULL,
    [Company] nvarchar(max)  NULL,
    [Telephone] nvarchar(max)  NULL
);
GO

-- Creating table 'Technologies'
CREATE TABLE [dbo].[Technologies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NULL
);
GO

-- Creating table 'TechnologySteps'
CREATE TABLE [dbo].[TechnologySteps] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Step] nvarchar(max)  NULL,
    [TechnologyId] int  NOT NULL,
    [StepNumber] int  NOT NULL
);
GO

-- Creating table 'QuoteProcessTracks'
CREATE TABLE [dbo].[QuoteProcessTracks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ProcessStepId] nvarchar(max)  NOT NULL,
    [DateTime] nvarchar(max)  NOT NULL,
    [QuoteId] int  NOT NULL,
    [UserId] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Quotes'
CREATE TABLE [dbo].[Quotes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ClientId] int  NOT NULL,
    [QuoteReferenceNumber] nvarchar(max)  NOT NULL,
    [QuoteDate] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [AssignedTo] nvarchar(max)  NOT NULL,
    [AssignedBy] nvarchar(max)  NOT NULL,
    [ProcessStepId] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Costs'
CREATE TABLE [dbo].[Costs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CostTypeId] int  NOT NULL,
    [CostName] nvarchar(max)  NOT NULL,
    [CostDescription] nvarchar(max)  NOT NULL,
    [CostValue] float  NOT NULL
);
GO

-- Creating table 'CostTypes'
CREATE TABLE [dbo].[CostTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'LineItems'
CREATE TABLE [dbo].[LineItems] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [QuoteId] int  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [Hours] int  NOT NULL,
    [TotalPrice] float  NOT NULL,
    [TechnologyStepId] int  NOT NULL,
    [Rate] float  NOT NULL,
    [RateType] nvarchar(max)  NOT NULL,
    [NormalLevel] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'VendorsPerTechnologies'
CREATE TABLE [dbo].[VendorsPerTechnologies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TechnologyId] int  NOT NULL,
    [VendorCount] int  NOT NULL,
    [QuoteId] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'AdditionalSteps'
CREATE TABLE [dbo].[AdditionalSteps] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [CostTypeId] int  NOT NULL
);
GO

-- Creating table 'AdditionalLineItems'
CREATE TABLE [dbo].[AdditionalLineItems] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [QuoteId] int  NOT NULL,
    [AdditionalStepId] int  NOT NULL,
    [HrsQty] nvarchar(max)  NOT NULL,
    [TotalPrice] float  NOT NULL,
    [Rate] float  NOT NULL,
    [RateType] nvarchar(max)  NOT NULL,
    [Level] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Clients'
ALTER TABLE [dbo].[Clients]
ADD CONSTRAINT [PK_Clients]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Technologies'
ALTER TABLE [dbo].[Technologies]
ADD CONSTRAINT [PK_Technologies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TechnologySteps'
ALTER TABLE [dbo].[TechnologySteps]
ADD CONSTRAINT [PK_TechnologySteps]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'QuoteProcessTracks'
ALTER TABLE [dbo].[QuoteProcessTracks]
ADD CONSTRAINT [PK_QuoteProcessTracks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Quotes'
ALTER TABLE [dbo].[Quotes]
ADD CONSTRAINT [PK_Quotes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Costs'
ALTER TABLE [dbo].[Costs]
ADD CONSTRAINT [PK_Costs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CostTypes'
ALTER TABLE [dbo].[CostTypes]
ADD CONSTRAINT [PK_CostTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LineItems'
ALTER TABLE [dbo].[LineItems]
ADD CONSTRAINT [PK_LineItems]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VendorsPerTechnologies'
ALTER TABLE [dbo].[VendorsPerTechnologies]
ADD CONSTRAINT [PK_VendorsPerTechnologies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AdditionalSteps'
ALTER TABLE [dbo].[AdditionalSteps]
ADD CONSTRAINT [PK_AdditionalSteps]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AdditionalLineItems'
ALTER TABLE [dbo].[AdditionalLineItems]
ADD CONSTRAINT [PK_AdditionalLineItems]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------