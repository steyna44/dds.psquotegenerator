﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDS.Core.DataLayer
{
    public partial class DefaultConnectionContext : DbContext
    {
        public DefaultConnectionContext()
            : base("name=DefaultConnection")
        {
            
        }
    }
}
