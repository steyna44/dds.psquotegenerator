//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DDS.Core.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class ManualStep
    {
        public ManualStep()
        {
            this.ManualLineItems = new HashSet<ManualLineItem>();
        }
    
        public int Id { get; set; }
        public int StepNumber { get; set; }
        public string StepDescription { get; set; }
        public int StepCategory { get; set; }
    
        public virtual ICollection<ManualLineItem> ManualLineItems { get; set; }
        public virtual Category Category { get; set; }
    }
}
