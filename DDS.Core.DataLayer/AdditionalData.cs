//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DDS.Core.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class AdditionalData
    {
        public int Id { get; set; }
        public int Nodes { get; set; }
        public int VLANs { get; set; }
        public int SSIDs { get; set; }
        public int QuoteId { get; set; }
        public int TechnologyId { get; set; }
    
        public virtual Quote Quote { get; set; }
        public virtual Technology Technology { get; set; }
    }
}
