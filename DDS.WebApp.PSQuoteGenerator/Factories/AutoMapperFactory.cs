﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using DDS.Core.BusinessObjects.Technology;
using DDS.Core.DataLayer;
using DDS.WebApp.PSQuoteGenerator.Models;

namespace DDS.WebApp.PSQuoteGenerator.Factories
{
    public static class AutoMapperFactory
    {
        public static void CreateMaps()
        {
            //Technology
            Mapper.CreateMap<Technology, TechnologyModel>();
            Mapper.CreateMap<TechnologyModel, Technology>();

            //Category
            Mapper.CreateMap<Category, CategoryModel>();
            Mapper.CreateMap<CategoryModel, Category>();

            //Client
            Mapper.CreateMap<Client, ClientModel>();
            Mapper.CreateMap<ClientModel, Client>();

            //Engineer
            Mapper.CreateMap<Engineer, EngineerModel>();
            Mapper.CreateMap<EngineerModel, Engineer>();

            //Manager
            Mapper.CreateMap<Manager, ManagerModel>();
            Mapper.CreateMap<ManagerModel, Manager>();

            //Step
            Mapper.CreateMap<Step, StepModel>();
            Mapper.CreateMap<StepModel, Step>();

            //ComplexStep
            Mapper.CreateMap<ComplexStep, ComplexStepModel>();
            Mapper.CreateMap<ComplexStepModel, ComplexStep>();

            //Manual Step
            Mapper.CreateMap<ManualStep, ManualStepModel>();
            Mapper.CreateMap<ManualStepModel, ManualStep>();

            //Quote
            Mapper.CreateMap<Quote, QuoteModel>();
            Mapper.CreateMap<QuoteModel, Quote>();

            //Quote Complexity
            Mapper.CreateMap<ComplexityObj, QuoteTechnologyComplexity>();
            Mapper.CreateMap<QuoteTechnologyComplexity, ComplexityObj>();

            //SundryCost
            Mapper.CreateMap<SundryCost, SundryCostModel>();
            Mapper.CreateMap<SundryCostModel, SundryCost>();

            //PMDeliverables
            Mapper.CreateMap<PMDeliverable, PMDeliverableModel>();
            Mapper.CreateMap<PMDeliverableModel, PMDeliverable>();
        }
    }
}