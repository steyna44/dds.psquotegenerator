﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DDS.Core.DataLayer;
using DDS.WebApp.PSQuoteGenerator.App_Start;
using DDS.WebApp.PSQuoteGenerator.Factories;
using System.Data.Entity;

namespace DDS.WebApp.PSQuoteGenerator
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Database.SetInitializer<DataManagementContext>(null);

            AutoMapperFactory.CreateMaps();
        }


        //protected void Application_AcquireRequestState(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        var lang = HttpContext.Current.Session["CurrentCurrency"] as string;
        //        if (!string.IsNullOrEmpty(lang))
        //        {
        //            var culture = CultureInfo.GetCultureInfo(lang);

        //            Thread.CurrentThread.CurrentUICulture = culture;
        //            Thread.CurrentThread.CurrentCulture = culture;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        var hello = "";
        //    }
            
        //}
    }
}
