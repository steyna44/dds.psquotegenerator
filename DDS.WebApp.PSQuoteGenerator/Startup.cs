﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DDS.WebApp.PSQuoteGenerator.Startup))]
namespace DDS.WebApp.PSQuoteGenerator
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
