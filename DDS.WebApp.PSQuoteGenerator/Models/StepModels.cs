﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DDS.Core.DataLayer;

namespace DDS.WebApp.PSQuoteGenerator.Models
{
    public class StepModel
    {
        public int Id { get; set; }
        
        [Display(Name="Step Number")]
        public int StepNumber { get; set; }

        [Required]
        [Display(Name="Description")]
        public string StepDescription { get; set; }

        [Required]
        [Display(Name = "Engineer Level")]
        public int EngineeringLevel { get; set; }

        [Required]
        public string Hours { get; set; }

        [Required]
        public int Technology { get; set; }

        [Required]
        [Display(Name = "Project Type")]
        public int ProjectType { get; set; }

        public virtual Technology Tech { get; set; }
        public virtual Engineer Eng { get; set; }
        public virtual ProjectType ProjectType1 { get; set; }
    }

    public class ManualStepModel
    {
        public int Id { get; set; }

        [Display(Name = "Step Number")]
        public int StepNumber { get; set; }

        [Required]
        [Display(Name = "Step Description")]
        public string StepDescription { get; set; }
        [Required]
        [Display(Name = "Step Category")]
        public int StepCategory { get; set; }
        public virtual Category Category { get; set; }
    }

    public class ManualStepContinuedModel
    {
        public int Id { get; set; }
        [Display(Name = "Step Description")]
        public string StepDescription { get; set; }
        [Display(Name = "Step Category")]
        public int StepCategory { get; set; }
        [Display(Name = "Technology")]
        public string TechName { get; set; }
        public int ManualStepId { get; set; }
        [Display(Name = "Engineer Level")]
        public int EngineerLevel { get; set; }
        public string Hours { get; set; }
        public bool Include { get; set; }
        public int QuoteId { get; set; }
    }

    public class AdditionalLineItemsModel
    {
        public int Id { get; set; }
        public int QuoteId { get; set; }
        public int PMDeliverableId { get; set; }
        public int ManagerLevel { get; set; }
        public string Hours { get; set; }
        public bool Include { get; set; }
    }

    public class SundryLineItemsModel
    {
        public int Id { get; set; }
        public int QuoteId { get; set; }
        public int SundryCostId { get; set; }
        public string Quantity { get; set; }
        public bool Include { get; set; }
    }
}