﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DDS.WebApp.PSQuoteGenerator.Models
{
    public class PMDeliverableModel
    {
        public int Id { get; set; }
        public int? StepNumber { get; set; }
        [Required]
        public string Description { get; set; }
    }
}