﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DDS.WebApp.PSQuoteGenerator.Models
{
    public class EngineerModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name="Engineer Level")]
        public string EngineeringLevel { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        [Display(Name="Hourly Rate")]
        public decimal HourlyRate { get; set; }
    }
}