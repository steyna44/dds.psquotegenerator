﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DDS.Core.DataLayer;

namespace DDS.WebApp.PSQuoteGenerator.Models
{
    public class ComplexStepModel
    {
        public int Id { get; set; }

        [Display(Name = "Step Number")]
        public int StepNumber { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string StepDescription { get; set; }

        [Required]
        [Display(Name = "Engineer Level")]
        public int EngineeringLevel { get; set; }

        [Required]
        public int Hours { get; set; }

        [Required]
        public int Technology { get; set; }

        [Required]
        [Display(Name = "Project Type")]
        public int ProjectType { get; set; }

        [Required]
        [Display(Name = "Step Type")]
        public int StepType { get; set; }

        [Required]
        [Display(Name = "Hour Type")]
        public int HourType { get; set; }

        public virtual ComplexStepType ComplexStepType { get; set; }
        public virtual Engineer Engineer { get; set; }
        public virtual ProjectType ProjectType1 { get; set; }
        public virtual Technology Technology1 { get; set; }
        public virtual HourType HourType1 { get; set; }
    }

    public class ComplexStepContinued
    {
        public int Id { get; set; }
        [Display(Name = "Step Description")]
        public string StepDescription { get; set; }
        [Display(Name = "Step Category")]
        public int StepCategory { get; set; }
        [Display(Name = "Technology")]
        public string TechName { get; set; }
        public int ComplexStepId { get; set; }
        [Display(Name = "Engineer Level")]
        public string EngineerLevel { get; set; }
        public string ProjectType { get; set; }
        public int Value1 { get; set; }
        public int Value2 { get; set; }
        public bool Include { get; set; }
        public int QuoteId { get; set; }
    }
}