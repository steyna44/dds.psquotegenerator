﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DDS.Core.DataLayer;

namespace DDS.WebApp.PSQuoteGenerator.Models
{
    public class QuoteModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name="Quote Name")]
        public string QuoteName { get; set; }

        [Required]
        [Display(Name = "Quote Description")]
        public string QuoteDesc { get; set; }
        
        [Display(Name="Proposal")]
        public string ProposalPath { get; set; }

        public string Status { get; set; }

        [Required]
        public int Client { get; set; }

        [Display(Name="Quote Date")]
        public string QuoteDate { get; set; }

        [Required]
        [Display(Name = "Reference Number")]
        public string QuoteRefNumber { get; set; }

        public string ProjectType { get; set; }

        public virtual Client LinkedClient { get; set; }
        public virtual ICollection<LineItem> LineItems { get; set; }
    }

    public class QuoteTechnologyModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name="Technology")]
        public List<int> SelectedTechnologies { get; set; }
    }

    public class QuoteTechnologyComplexity
    {
        public int Id { get; set; }

        public int TechId { get; set; }

        public string TechName { get; set; }
        public List<ProjectType> ProjectTypes { get; set; } 
        public List<string> Options { get; set; } 
    }

}