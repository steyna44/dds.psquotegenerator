﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DDS.WebApp.PSQuoteGenerator.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name="Category")]
        public string CategoryName { get; set; }
    }
}