﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDS.WebApp.PSQuoteGenerator.Models
{
    public class CurrencyModel
    {
        public string CultureCode { get; set; }
        public string CurrentController { get; set; }
        public string CurrentView { get; set; }
    }
}