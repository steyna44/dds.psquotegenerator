﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDS.Core.DataLayer;

namespace DDS.WebApp.PSQuoteGenerator.Models
{
    public class AdditionalDataModel
    {
        public int Id { get; set; }
        public int Nodes { get; set; }
        public int VLANs { get; set; }
        public int SSIDs { get; set; }
        public int QuoteId { get; set; }
        public int TechnologyId { get; set; }

        public virtual Quote Quote { get; set; }
        public virtual Technology Technology { get; set; }
    }
}