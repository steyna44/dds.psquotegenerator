﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DDS.WebApp.PSQuoteGenerator.Models
{
    public class SundryCostModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name="Cost Description")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Cost Type")]
        public string Type { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal Rate { get; set; }
    }
}