﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DDS.Core.BusinessObjects.Technology;
using DDS.Core.DataLayer;

namespace DDS.WebApp.PSQuoteGenerator.Models
{
    public class TechnologyModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name="Technology Name")]
        public string TechnologyName { get; set; }
        [Required]
        [Display(Name = "Technology Category")]
        public int TechnologyCategory { get; set; }
        public virtual Category Category { get; set; }
    }
}