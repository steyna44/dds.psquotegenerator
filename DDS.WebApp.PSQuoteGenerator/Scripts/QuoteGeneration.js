﻿$(document).ready(function() {
    $("#PMTOTAL").html($("#hiddenCurrency").val() + " " + addCommas(parseFloat($("#hiddenPmTotal").val()).toFixed(2)));
    
    $("[name='my-checkbox']").bootstrapSwitch({
        onText: 'Approved',
        offText: 'Declined',
        size: 'medium',
        onColor: 'success'
    });
    
    $('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state == true) {
            $("#decline").hide("slow");
            $("#decline_reason").prop("disabled", true);
        } else {
            $("#decline").show("slow");
            $("#decline_reason").prop("disabled", false);
            $("#decline_reason").val("");
        }
    });
    
    $("#submit_button").on("click", function () {
        if ($('input[name="my-checkbox"]').is(":checked")) {
            BootstrapDialog.confirm('Are you sure you would like to approve and finalise this quote?', function (result) {
                if (result) {
                    $("#real_submit").click();

                } else {

                }
            });
        } else {
            if ($("#decline_reason").val() == "") {
                BootstrapDialog.alert("Please provide a reason for declining this qoute");
            } else {
                BootstrapDialog.confirm('You are about to assign this task to its previous owner to correct cost allocations.<br /> Do you wish to continue?', function (result) {
                    if (result) {
                        $("#real_submit").click();

                    } else {

                    }
                });
            }
        }
    });
});

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}