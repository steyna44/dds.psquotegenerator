﻿$(document).ready(function() {
    $("#assignment").hide();
    $("#AssignedTo").prop("disabled", true);
    
    $("[name='my-checkbox']").bootstrapSwitch({
        onText: 'Approved',
        offText: 'Declined',
        size: 'medium',
        onColor: 'success'
    });

    $('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state == true) {
            $("#assignment").show("slow");
            $("#decline").hide("slow");
            $("#AssignedTo").prop("disabled", false);
            $("#decline_reason").prop("disabled", true);
        } else {
            $("#decline").show("slow");
            $("#assignment").hide("slow");
            $("#AssignedTo").prop("disabled", true);
            $("#decline_reason").prop("disabled", false);
            $("#decline_reason").val("");
        }
    });


    $("#submit_button").on("click", function() {
        if ($('input[name="my-checkbox"]').is(":checked")) {
            var assignedto = $("#AssignedTo option:selected").text();
            BootstrapDialog.confirm('You are about to assign this task to <strong>' + assignedto + '</strong> for cost allocation.<br /> Do you wish to continue?', function (result) {
                if (result) {
                    $("#real_submit").click();

                } else {

                }
            });
        } else {
            if ($("#decline_reason").val() == "") {
                BootstrapDialog.alert("Please provide a reason for declining the selected steps");
            } else {
                BootstrapDialog.confirm('You are about to assign this task to its previous owner to reselect relevant steps.<br /> Do you wish to continue?', function (result) {
                    if (result) {
                        $("#real_submit").click();

                    } else {

                    }
                });
            }
        }
    });
});