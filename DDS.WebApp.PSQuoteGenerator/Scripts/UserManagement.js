﻿function showPasswordModal(username, name) {
    $('#changePasswordForm').bootstrapValidator({
        message: 'The passwords you entered are not valid.',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        live: 'enabled',
        fields: {
            newPassword: {
                validators: {
                    stringLength: {
                        min: 6,
                        message: 'The password must be longer than 6 characters'
                    }
                }
            },
            confirmPassword: {
                validators: {
                    
                    identical: {
                        field: 'newPassword',
                        message: 'The passwords dont match'
                    }
                }
            }
        }
    });
    $('#changePasswordForm').data('bootstrapValidator').setLiveMode('enabled');

    $('#password_user').html(name);
    $("#input_username").val(username);
    $('#modal_password_change').modal('show');
}

function checkPasswords() {
    var newPass = $("#newPassword");
    var confirmPass = $("#confirmPassword");

    if (newPass.val() == confirmPass.val()) {
        $("#div_confirm_password").removeClass("has-error").addClass("has-success");
        $("#icon_confirm_password").removeClass("glyphicon-remove").addClass("glyphicon-ok");
        $("#btn_save_password").attr("disabled", false);
    } else {
        $("#div_confirm_password").removeClass("has-success").addClass("has-error");
        $("#icon_confirm_password").removeClass("glyphicon-ok").addClass("glyphicon-remove");
        $("#btn_save_password").attr("disabled", true);
    }
}

function saveNewPassword() {
    if ($('#changePasswordForm').data('bootstrapValidator').isValid()) {
        console.log("Now saving new password");
        var newPass = $("#newPassword").val();
        var username = $("#input_username").val();

        console.log("Username: " + username + ", New Password:" + newPass);
        $("#msg_error").remove();
        $("#newPassword").val("");
        $("#confirmPassword").val("");
        $('#changePasswordForm').data('bootstrapValidator').resetForm();

        $.getJSON("/Account/ChangePassword", { username: username, newPassword: newPass }, function(data) {
            console.log(data);
            if (data == "Password Successfully Changed.") {
                $('#modal_password_change').modal('hide');
            } else {
                var msgContainer = $("<p id='msg_error' class='bg-danger'/>");
                msgContainer.append("There was a problem changing your password. Please try again");
                $('#modal_password_change').append(msgContainer);
            }
        });
    } else {
        $('#changePasswordForm').data('bootstrapValidator').validate();
    }
}

function getRegions(selectedRegions) {
    var selectedClient = $("#CompanyId").val();
    var url = "/Account/GetRegions/" + selectedClient;

    $.getJSON(url, function(data) {
        var dropdown = $("#Regions");
        dropdown.empty();
        //dropdown.append("<option value='0'>All Regions</option>");

        $.each(data, function (i, item) {
            var option = $("<option/>");
            if (selectedRegions != undefined && selectedRegions != null && selectedRegions.indexOf(parseInt(i)) > -1)
                option.attr("selected",true);
            
            option.attr("value", i);
            option.html(item);

            dropdown.append(option);
        });
    });
}