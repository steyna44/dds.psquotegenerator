﻿$(document).ready(function () {
    var Currency = $("#currency").val();
    var check = Currency + " " + "0";
    $(".cost-type").each(function () {
        var amount = this.value.split("-").pop();
        //console.log(this.value + " NEW " + myString);

        var currentId = this.id.split("-").pop();

        if ($("#rate-" + currentId).val() == check && $("#total-" + currentId).val() == check && $("#hours-" + currentId).val() == 0) {
            $("#rate-" + currentId).val(Currency + " " + amount);
            $("#hours-" + currentId).val("1");
            $("#total-" + currentId).val(Currency + " " + amount);
        }
        
    });
    
    $(".PMcost-type").each(function () {
        var amount = this.value.split("-").pop();
        //console.log(this.value + " NEW " + myString);

        var currentId = this.id.split("-").pop();

        if ($("#PMrate-" + currentId).val() == check && $("#PMtotal-" + currentId).val() == check && $("#PMhours-" + currentId).val() == "") {
            $("#PMrate-" + currentId).val("");
            $("#PMhours-" + currentId).val("");
            $("#PMtotal-" + currentId).val("");
        }

    });
    
    $(".travelrate").each(function () {

        var currentId = this.id.split("-").pop();

        if ($("#travelrate-" + currentId).val() == check && $("#traveltotal-" + currentId).val() == check && $("#travelhours-" + currentId).val() == "") {
            $("#travelrate-" + currentId).val("");
            $("#travelhours-" + currentId).val("");
            $("#traveltotal-" + currentId).val("");
        }

    });
    
    $(".otherrate").each(function () {

        var currentId = this.id.split("-").pop();

        if ($("#otherrate-" + currentId).val() == check && $("#othertotal-" + currentId).val() == check && $("#otherhours-" + currentId).val() == "") {
            $("#otherrate-" + currentId).val("");
            $("#otherhours-" + currentId).val("");
            $("#othertotal-" + currentId).val("");
        }

    });

    $(".cost-type").on("change", function () {
        var currentLevel = this.value.substr(0, this.value.indexOf('-'));
        var currentId = this.id.split("-").pop();
        var baseValue = this.value.split("-").pop();
        var currentRateType = $("#rateType-" + currentId).val(); // 1, 2 or 3
        var currentQTY = $("#hours-" + currentId).val();
        var total = 0;
        var rate = 0;
        //console.log("currentId: " + currentId + " baseValue: " + baseValue + " currentRateType: " + currentRateType + " currentQTY: " + currentQTY);
        
        if (currentRateType == "1") {
            rate = baseValue;
            total = baseValue * currentQTY;
        } else if (currentRateType == "2") {
                rate = parseFloat(baseValue) + parseFloat(baseValue/2);
                total = rate * currentQTY;
        } else if (currentRateType == "3") {
                rate = parseFloat(baseValue)*2 ;
                total = rate * currentQTY;
        }
        
        $("#rate-" + currentId).val(Currency + " " + rate);
        $("#total-" + currentId).val(Currency + " " + total);
    });
    
    $(".rateType").on("change", function () {
        var currentId = this.id.split("-").pop();
        var currentLevel = $("#NormalCost-" + currentId).val().substr(0, $("#NormalCost-" + currentId).val().indexOf('-'));
        var baseValue = $("#NormalCost-" + currentId).val().split("-").pop();
        var currentRateType = $("#rateType-" + currentId).val(); // 1, 2 or 3
        var currentQTY = $("#hours-" + currentId).val();
        var total = 0;
        var rate = 0;
        console.log("currentId: " + currentId + " baseValue: " + baseValue + " currentRateType: " + currentRateType + " currentQTY: " + currentQTY + " currentLevel: " + currentLevel);

        if (currentRateType == "1") {
            rate = baseValue;
            total = baseValue * currentQTY;
        } else if (currentRateType == "2") {
            rate = parseFloat(baseValue) + parseFloat(baseValue / 2);
            total = rate * currentQTY;
        } else if (currentRateType == "3") {
            rate = parseFloat(baseValue) * 2;
            total = rate * currentQTY;
        }

        $("#rate-" + currentId).val(Currency + " " + rate);
        $("#total-" + currentId).val(Currency + " " + total);
    });
    


    $(".hours").on("change", function() {
        var currentId = this.id.split("-").pop();
        var baseValue = $("#NormalCost-" + currentId).val().split("-").pop();
        var rate = $("#rate-" + currentId).val().replace(Currency + " ","");
        var currentRateType = $("#rateType-" + currentId).val(); // 1, 2 or 3
        var currentQTY = $("#hours-" + currentId).val();
        var total = 0;

        total = rate * currentQTY;
        $("#total-" + currentId).val(Currency + " " + total);
    });
    
    $(".PMcost-type").on("change", function () {
        var currentLevel = this.value.substr(0, this.value.indexOf('-'));
        var currentId = this.id.split("-").pop();
        var baseValue = this.value.split("-").pop();
        var currentRateType = $("#PMrateType-" + currentId).val(); // 1, 2 or 3
        var currentQTY = $("#PMhours-" + currentId).val();
        var total = 0;
        var rate = 0;
        //console.log("currentId: " + currentId + " baseValue: " + baseValue + " currentRateType: " + currentRateType + " currentQTY: " + currentQTY);

        if (currentRateType == "1") {
            if (currentLevel != "") {
                rate = baseValue;
                total = baseValue * currentQTY;
            } else {
                rate = "";
                total = "";
            }
        } else if (currentRateType == "2") {
            if (currentLevel != "") {
                rate = parseFloat(baseValue) + parseFloat(baseValue / 2);
                total = rate * currentQTY;
            } else {
                rate = "";
                total = "";
            }
        } else if (currentRateType == "3") {
            if (currentLevel != "") {
                rate = parseFloat(baseValue) * 2;
                total = rate * currentQTY;
            } else {
                rate = "";
                total = "";
            }
        }

        if (currentLevel != "") {
            $("#PMrate-" + currentId).val(Currency + " " + rate);
            $("#PMtotal-" + currentId).val(Currency + " " + total);
        } else {
            $("#PMrate-" + currentId).val("");
            $("#PMtotal-" + currentId).val("");
        }
    });

    $(".PMrateType").on("change", function () {
        var currentId = this.id.split("-").pop();
        var currentLevel = $("#PMDeliverableCost-" + currentId).val().substr(0, $("#PMDeliverableCost-" + currentId).val().indexOf('-'));
        var baseValue = $("#PMDeliverableCost-" + currentId).val().split("-").pop();
        var currentRateType = $("#PMrateType-" + currentId).val(); // 1, 2 or 3
        var currentQTY = $("#PMhours-" + currentId).val();
        var total = 0;
        var rate = 0;

        if (currentRateType == "1") {
            if (currentLevel != "") {
                rate = baseValue;
                total = baseValue * currentQTY;
            } else {
                rate = "";
                total = "";
            }
        } else if (currentRateType == "2") {
            if (currentLevel != "") {
                rate = parseFloat(baseValue) + parseFloat(baseValue / 2);
                total = rate * currentQTY;
            } else {
                rate = "";
                total = "";
            }
        } else if (currentRateType == "3") {
            if (currentLevel != "") {
                rate = parseFloat(baseValue) * 2;
                total = rate * currentQTY;
            } else {
                rate = "";
                total = "";
            }
        }

        if (currentLevel != "") {
            $("#PMrate-" + currentId).val(Currency + " " + rate);
            $("#PMtotal-" + currentId).val(Currency + " " + total);
        } else {
            $("#PMrate-" + currentId).val("");
            $("#PMtotal-" + currentId).val("");
        }
    });



    $(".PMhours").on("change", function () {
        var currentId = this.id.split("-").pop();
        var currentLevel = $("#PMDeliverableCost-" + currentId).val().substr(0, $("#PMDeliverableCost-" + currentId).val().indexOf('-'));
        var baseValue = $("#PMDeliverableCost-" + currentId).val().split("-").pop();
        var rate = $("#PMrate-" + currentId).val().replace(Currency + " ", "");
        var currentRateType = $("#PMDeliverableCost-" + currentId).val(); // 1, 2 or 3
        var currentQTY = $("#PMhours-" + currentId).val();
        var total = 0;

        if (currentLevel != "") {
            total = rate * currentQTY;
            $("#PMtotal-" + currentId).val(Currency + " " + total);
        } else {
            $("#PMtotal-" + currentId).val("");
        }
    });
    
    $(".travelhours").on("change", function () {
        var currentId = this.id.split("-").pop();
        var rate = $("#travelrate-" + currentId).val().replace(Currency + " ", "");
        var currentQTY = $("#travelhours-" + currentId).val();
        var total = 0;


        total = rate * currentQTY;
        $("#traveltotal-" + currentId).val(Currency + " " + total);

    });
    
    $(".travelrate").on("change", function () {
        var currentId = this.id.split("-").pop();
        var rate = $("#travelrate-" + currentId).val().replace(Currency + " ", "");
        
        var currentQTY = $("#travelhours-" + currentId).val();
        var total = 0;


        total = rate * currentQTY;
        if (isNaN(rate) || isNaN(currentQTY)) {
            $("#travelhours-" + currentId).val(0);
            $("#travelrate-" + currentId).val(Currency + " 0");
            $("#traveltotal-" + currentId).val(Currency + " 0");
        } else {
            $("#travelrate-" + currentId).val(Currency + " " + rate);
            $("#traveltotal-" + currentId).val(Currency + " " + total);
        }
        
        
    });
    
    $(".otherhours").on("change", function () {
        var currentId = this.id.split("-").pop();
        var rate = $("#otherrate-" + currentId).val().replace(Currency + " ", "");
        var currentQTY = $("#otherhours-" + currentId).val();
        var total = 0;


        total = rate * currentQTY;
        $("#othertotal-" + currentId).val(Currency + " " + total);

    });

    $(".otherrate").on("change", function () {
        var currentId = this.id.split("-").pop();
        var rate = $("#otherrate-" + currentId).val().replace(Currency + " ", "");
        var currentQTY = $("#otherhours-" + currentId).val();
        var total = 0;


        total = rate * currentQTY;
        if (isNaN(rate) || isNaN(currentQTY)) {
            $("#otherhours-" + currentId).val(0);
            $("#otherrate-" + currentId).val(Currency + " 0");
            $("#othertotal-" + currentId).val(Currency + " 0");
        } else {
            $("#otherrate-" + currentId).val(Currency + " " + rate);
            $("#othertotal-" + currentId).val(Currency + " " + total);
        }
    });

    $("#action").on("change", function() {
        if ($("#action").val() == "3") {
            $("#assignment").show("slow");
        } else {
            $("#assignment").hide("slow");
        }
    });

    $("#submit_button").on("click", function() {
        if ($("#action").val() == "0") {
            BootstrapDialog.alert('Please select an action first');
        } else if ($("#action").val() == "1") {
            BootstrapDialog.confirm('Your changes will be saved for later', function (result) {
                if (result) {
                    $("#real_submit").click();
                } else {
                    
                }
            });
        } else if ($("#action").val() == "3") {
            var assignedTo = $("#AssignedTo :selected").html();
            BootstrapDialog.confirm('You are about to assign this task to <strong>' + assignedTo + '</strong> for final approval.<br />Are you sure all costs have been allocated correctly?', function (result) {
                if (result) {
                    $("#real_submit").click();
                } else {
                    
                }
            });
        }
    });
});
