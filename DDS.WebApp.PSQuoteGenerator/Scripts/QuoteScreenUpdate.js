﻿$(document).ready(function () {
    $("#tech-group").hide();
    $.fn.bootstrapSwitch.defaults.offText = "No";
    $.fn.bootstrapSwitch.defaults.onText = "Yes";
    $.fn.bootstrapSwitch.defaults.onColor = "success";

    $("[name='my-checkbox']").bootstrapSwitch();

    $('[name="my-checkbox"]').bootstrapSwitch('toggleDisabled');

    var currentcheckbox = "";
    var errors = 0;

    $("#QuoteForm input[type=checkbox].technology-checkbox").on("change", function () {
        if (jQuery('#QuoteForm input[type=checkbox]:checked.technology-checkbox').length) {
            $('[name="my-checkbox"]').bootstrapSwitch('disabled', false, false);
        } else {
            $('[name="my-checkbox"]').bootstrapSwitch('disabled', true, true);
        }
    });

    $('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function (event, state) {
        if (jQuery('#QuoteForm input[type=checkbox]:checked.technology-checkbox').length) {
            if (state == true) {
                $(".technology-checkbox").prop('disabled', true);
                $(".technology-label").css("color", "grey");
                $(".technology-label").css("cursor", "not-allowed");
                //Mutiple Vendors YES
                $(".technology-label").each(function (index, element) {
                    console.log(element.id);
                    currentcheckbox = element.id.slice(0, -6);
                    console.log(currentcheckbox);
                    if ($("#" + currentcheckbox).is(':checked')) {
                        //$("#" + element.id).after("<input type='text' id='vendorAmount-" + element.id + "' class='dynamix' />");
                        $("#Technology-Group").append("<span class='dynamix input-group-addon'>" + $("#" + element.id).html() + "</span>" +
                                                      "<input type='number' id='vendorAmount-" + currentcheckbox + "' name='vendorAmount-" + currentcheckbox + "' class='dynamix form-control text-box single-line' value='2' min='2' max='10' required step='1' />");
                    }
                });

                $("#tech-group").show("slow");

            } else if (state == false) {
                $(".technology-checkbox").prop('disabled', false);
                $(".technology-label").css("color", "#333333");
                $(".technology-label").css("cursor", "default");
                $("#tech-group").hide("slow", function () {
                    $(".dynamix").remove();
                });
            }
        }

    });

    $("#submit").on("click", function () {

        if (jQuery('#QuoteForm input[type=checkbox]:checked.technology-checkbox').length) {
            var currentAssignee = $("#AssignedTo option:selected").text();

            BootstrapDialog.confirm('You are about to re-assign this task to ' + '<strong>' + currentAssignee + '</strong>' + '<br> <strong>PLEASE NOTE</strong><br>By doing this you will be removing all previously saved data for this quote.<br> do you wish to continue?', function (result) {
                if (result) {
                    $("#hiddensubmit").click();

                } else {

                }
            });


        } else {

            BootstrapDialog.alert("Please ensure that atleast one technology type is selected");

        }



    });

    $("#QuoteForm").on("submit", function () {
        if ($(".text-box").val() != "") {
            $(".technology-checkbox").prop('disabled', false);
        }

    });
});
