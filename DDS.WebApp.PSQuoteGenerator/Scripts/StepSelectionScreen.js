﻿$(document).ready(function() {
    
    $("#submit_button").on("click", function () {
        
        var itemArray = [];
        $(".panel-default").each(function () {
            //console.log("hello");
            //var checkboxes = $('#divID :checkbox');
            
            var checkboxes = $("#" + this.id + " :checkbox").filter(':checked').length;
            console.log(this.id + " has " + checkboxes + " checked checkboxes");

            itemArray.push(checkboxes);
        });
        //console.log(itemArray);
        if (jQuery.inArray(0, itemArray) != -1) {
            BootstrapDialog.alert('Please ensure that atleast one step is selected per technology group');
            
        } else {
            var assignedto = $("#AssignedTo option:selected").text();
            BootstrapDialog.confirm('You are about to assign this task to <strong>'+assignedto+'</strong> for approval.<br /> Are you sure that the selected step(s) are correct?', function (result) {
                if (result) {
                    $("#real_submit").click();

                } else {

                }
            });
            
        }
        return false;
    });
});

function checkCheckboxes(id, pId) {

    switch ($("#" + id).html()) {
        case "check all":
            $('#' + pId).find(':checkbox').each(function(index,element) {
                $("#" + element.id).prop("checked",true);
            });
            $("#" + id).html("uncheck all");
            break;
        case "uncheck all":
            $('#' + pId).find(':checkbox').each(function (index, element) {
                $("#" + element.id).prop("checked", false);
            });
            $("#" + id).html("check all");
            break;
    }

}