﻿using System.Web.Mvc;

namespace DDS.WebApp.PSQuoteGenerator.App_Start
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
