﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DDS.Core.BusinessLayer.Methods.ProjectConfiguration;
using DDS.Core.DataLayer;
using DDS.WebApp.PSQuoteGenerator.Models;

namespace DDS.WebApp.PSQuoteGenerator.Controllers
{
    public class EngineerController : Controller
    {
        private readonly EngineerConfiguration _manager = new EngineerConfiguration();

        // GET: Engineer
        public ActionResult Index()
        {
            var model = Mapper.Map<List<Engineer>, List<EngineerModel>>(_manager.GetEngineers());

            return View(model);
        }

        // GET: Engineer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Engineer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,EngineeringLevel,HourlyRate")] EngineerModel engineerModel)
        {
            if (!ModelState.IsValid) return View(engineerModel);

            var model = Mapper.Map<EngineerModel, Engineer>(engineerModel);

            var inserted = _manager.InsertEngineer(model);

            if (inserted)
            {
                return RedirectToAction("Index");
            }
            return View(engineerModel);
        }

        // GET: Engineer/Edit/5
        public ActionResult Edit(int id)
        {
            var model = Mapper.Map<Engineer, EngineerModel>(_manager.GetEngineerById(id));

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Engineer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,EngineeringLevel,HourlyRate")] EngineerModel engineerModel)
        {
            if (!ModelState.IsValid) return View(engineerModel);
            var model = Mapper.Map<EngineerModel, Engineer>(engineerModel);

            var updated = _manager.UpdateEngineer(model);
            if (updated)
            {
                return RedirectToAction("Index");
            }
            return View(engineerModel);
        }

        // GET: Engineer/Delete/5
        public ActionResult Delete(int id)
        {
            var model = Mapper.Map<Engineer, EngineerModel>(_manager.GetEngineerById(id));

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Engineer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (_manager.RemoveEngineer(id))
            {
                return RedirectToAction("Index");
            }
            return View(id);
        }
    }
}
