﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DDS.Core.BusinessLayer.Methods.ProjectConfiguration;
using DDS.Core.DataLayer;
using DDS.WebApp.PSQuoteGenerator.Models;

namespace DDS.WebApp.PSQuoteGenerator.Controllers
{
    public class PMDeliverableController : Controller
    {
        private readonly PMDeliverableConfiguration _manager = new PMDeliverableConfiguration();

        // GET: PMDeliverableModels
        public ActionResult Index()
        {
            var model = Mapper.Map<List<PMDeliverable>, List<PMDeliverableModel>>(_manager.GetPMDeliverables());

            return View(model);
        }

        // GET: PMDeliverableModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PMDeliverableModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Description")] PMDeliverableModel pmDeliverableModel)
        {
            if (!ModelState.IsValid) return View(pmDeliverableModel);

            var model = Mapper.Map<PMDeliverableModel, PMDeliverable>(pmDeliverableModel);

            var inserted = _manager.InsertPMDeliverable(model);

            if (inserted)
            {
                return RedirectToAction("Index");
            }

            return View(pmDeliverableModel);
        }

        // GET: PMDeliverableModels/Edit/5
        public ActionResult Edit(int id)
        {
            var model = Mapper.Map<PMDeliverable, PMDeliverableModel>(_manager.GetPMDeliverableById(id));

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: PMDeliverableModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Description")] PMDeliverableModel pmDeliverableModel)
        {
            if (!ModelState.IsValid) return View(pmDeliverableModel);
            var model = Mapper.Map<PMDeliverableModel, PMDeliverable>(pmDeliverableModel);

            var updated = _manager.UpdatePMDeliverable(model);
            if (updated)
            {
                return RedirectToAction("Index");
            }
            return View(pmDeliverableModel);
        }

        // GET: PMDeliverableModels/Delete/5
        public ActionResult Delete(int id)
        {
            var model = Mapper.Map<PMDeliverable, PMDeliverableModel>(_manager.GetPMDeliverableById(id));

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: PMDeliverableModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (_manager.RemovePMDeliverable(id))
            {
                return RedirectToAction("Index");
            }
            return View(id);
        }
    }
}
