﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DDS.Core.BusinessLayer.Methods.ProjectConfiguration;
using DDS.Core.DataLayer;
using DDS.WebApp.PSQuoteGenerator.Models;

namespace DDS.WebApp.PSQuoteGenerator.Controllers
{
    public class ManagerController : Controller
    {
        private DataManagementContext db = new DataManagementContext();

        private readonly ManagerConfiguration _manager = new ManagerConfiguration();

        // GET: Manager
        public ActionResult Index()
        {
            var model = Mapper.Map<List<Manager>, List<ManagerModel>>(_manager.GetManagers());

            return View(model);
        }

        // GET: Manager/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Manager/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ManagerLevel,HourlyRate")] ManagerModel managerModel)
        {
            if (!ModelState.IsValid) return View(managerModel);

            var model = Mapper.Map<ManagerModel, Manager>(managerModel);

            var inserted = _manager.InsertManager(model);

            if (inserted)
            {
                return RedirectToAction("Index");
            }
            return View(managerModel);
        }

        // GET: Manager/Edit/5
        public ActionResult Edit(int id)
        {
            var model = Mapper.Map<Manager, ManagerModel>(_manager.GetManagerById(id));

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Manager/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ManagerLevel,HourlyRate")] ManagerModel managerModel)
        {
            if (!ModelState.IsValid) return View(managerModel);
            var model = Mapper.Map<ManagerModel, Manager>(managerModel);

            var updated = _manager.UpdateManager(model);
            if (updated)
            {
                return RedirectToAction("Index");
            }
            return View(managerModel);
        }

        // GET: Manager/Delete/5
        public ActionResult Delete(int id)
        {
            var model = Mapper.Map<Manager, ManagerModel>(_manager.GetManagerById(id));

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Manager/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (_manager.RemoveManager(id))
            {
                return RedirectToAction("Index");
            }
            return View(id);
        }
    }
}
