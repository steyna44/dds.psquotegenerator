﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DDS.Core.BusinessLayer.Methods.ProjectConfiguration;
using DDS.Core.DataLayer;
using DDS.WebApp.PSQuoteGenerator.Models;

namespace DDS.WebApp.PSQuoteGenerator.Controllers
{
    public class ClientController : Controller
    {
        private readonly ClientConfiguration _manager = new ClientConfiguration();

        // GET: Client
        public ActionResult Index()
        {
            var model = Mapper.Map<List<Client>, List<ClientModel>>(_manager.GetClients());

            return View(model);
        }

        // GET: Client/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Client/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Email,Company,Telephone")] ClientModel clientModel)
        {
            clientModel.CombinedName = clientModel.Company + " (" + clientModel.Name + ")";
            if (!ModelState.IsValid) return View(clientModel);

            
            var model = Mapper.Map<ClientModel, Client>(clientModel);

            var inserted = _manager.InsertClient(model);

            if (inserted)
            {
                return RedirectToAction("Index");
            }
            return View(clientModel);
        }

        // GET: Client/Edit/5
        public ActionResult Edit(int id)
        {
            var model = Mapper.Map<Client, ClientModel>(_manager.GetClientById(id));

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Client/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Email,Company,Telephone")] ClientModel clientModel)
        {
            clientModel.CombinedName = clientModel.Company + " (" + clientModel.Name + ")";
            if (!ModelState.IsValid) return View(clientModel);
            var model = Mapper.Map<ClientModel, Client>(clientModel);

            var updated = _manager.UpdateClient(model);
            if (updated)
            {
                return RedirectToAction("Index");
            }
            return View(clientModel);
        }

        // GET: Client/Delete/5
        public ActionResult Delete(int id)
        {
            var model = Mapper.Map<Client, ClientModel>(_manager.GetClientById(id));

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Client/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (_manager.RemoveClient(id))
            {
                return RedirectToAction("Index");
            }
            return RedirectToAction("Delete", new{id = id});
        }
    }
}
