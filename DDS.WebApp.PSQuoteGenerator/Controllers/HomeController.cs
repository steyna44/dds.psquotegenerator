﻿using System.Web.Mvc;

namespace DDS.WebApp.PSQuoteGenerator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {   
            return View();
        }
    }
}