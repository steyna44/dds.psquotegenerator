﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using DDS.Core.BusinessLayer.Methods.ProjectConfiguration;
using DDS.Core.DataLayer;
using DDS.WebApp.PSQuoteGenerator.Models;

namespace DDS.WebApp.PSQuoteGenerator.Controllers
{
    public class TechnologyController : Controller
    {
        private readonly TechnologyConfiguration _manager = new TechnologyConfiguration();

        // GET: Technology/Index
        public ActionResult Index()
        {
            var model = Mapper.Map<List<Technology>, List<TechnologyModel>>(_manager.GetTechnologies());

            return View(model);
        }

        //// POST: Technology/Index
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Index(FormCollection radioCollection)
        //{
        //    var selectedOption = radioCollection["optradio"];

        //    return RedirectToAction(selectedOption);
        //}

        // GET: Technology
        //public ActionResult ListTechnologies()
        //{
        //    var model = Mapper.Map<List<Technology>, List<TechnologyModel>>(_manager.GetTechnologies());

        //    return View(model);
        //}

        // GET: Technology/Create
        public ActionResult Create()
        {
            var categories = new CategoryConfiguration().GetCategories();

            ViewBag.TechnologyCategory = new SelectList(categories, "Id", "CategoryName");
            return View();
        }

        // POST: Technology/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TechnologyName,TechnologyCategory")] TechnologyModel technologyModel)
        {
            if (!ModelState.IsValid) return View(technologyModel);

            var model = Mapper.Map<TechnologyModel, Technology>(technologyModel);

            var inserted = _manager.InsertTechnology(model);

            if (inserted)
            {
                return RedirectToAction("Index");
            }

            var categories = new CategoryConfiguration().GetCategories();

            ViewBag.TechnologyCategory = new SelectList(categories, "Id", "CategoryName",technologyModel.TechnologyCategory);

            return View(technologyModel);
        }

        // GET: Technology/Edit/5
        public ActionResult Edit(int id)
        {
            var model = Mapper.Map<Technology, TechnologyModel>(_manager.GetTechnologyById(id));

            if (model == null)
            {
                return HttpNotFound();
            }

            var categories = new CategoryConfiguration().GetCategories();

            ViewBag.TechnologyCategory = new SelectList(categories, "Id", "CategoryName",model.TechnologyCategory);

            return View(model);
        }

        // POST: Technology/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TechnologyName,TechnologyCategory")] TechnologyModel technologyModel)
        {
            if (!ModelState.IsValid) return View(technologyModel);
            var model = Mapper.Map<TechnologyModel, Technology>(technologyModel);

            var updated = _manager.UpdateTechnology(model);
            if (updated)
            {
                return RedirectToAction("Index");
            }

            var categories = new CategoryConfiguration().GetCategories();

            ViewBag.TechnologyCategory = new SelectList(categories, "Id", "CategoryName",technologyModel.TechnologyCategory);

            return View(technologyModel);
        }

        // GET: Technology/Delete/5
        public ActionResult Delete(int id)
        {
            var model = Mapper.Map<Technology, TechnologyModel>(_manager.GetTechnologyById(id));

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Technology/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (_manager.RemoveTechnology(id))
            {
                return RedirectToAction("Index");
            }
            return View(id);
        }
    }
}
