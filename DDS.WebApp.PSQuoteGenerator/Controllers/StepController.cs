﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using DDS.Core.BusinessLayer.Methods.ProjectConfiguration;
using DDS.Core.DataLayer;
using DDS.WebApp.PSQuoteGenerator.Models;


namespace DDS.WebApp.PSQuoteGenerator.Controllers
{
    public class StepController : Controller
    {
        private readonly StepConfiguration _manager = new StepConfiguration();

        // GET: Step/Setup
        public ActionResult Setup()
        {
            ViewBag.Technology = new SelectList(_manager.Db.Technologies, "Id", "TechnologyName");
            ViewBag.ProjectType = new SelectList(_manager.GetProjectTypes(), "Id", "ProjectType1");

            return View();
        }

        // POST: Step/Setup
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Setup(FormCollection formCollection)
        {
            var tech = int.Parse(formCollection["Technology"]);
            System.Web.HttpContext.Current.Session["CurrentTechnologyId"] = tech;

            System.Web.HttpContext.Current.Session["CurrentTechnologyName"] = _manager.Db.Technologies.Find(tech).TechnologyName;

            var projType = int.Parse(formCollection["ProjectType"]);
            System.Web.HttpContext.Current.Session["CurrentProjectType"] = projType;

            System.Web.HttpContext.Current.Session["currentProjTypeString"] = _manager.Db.ProjectTypes.Find(projType).ProjectType1;

            return RedirectToAction("index");
        }

        // GET: Step
        public ActionResult Index()
        {

            var currentTech = System.Web.HttpContext.Current.Session["CurrentTechnologyId"] is int ? (int)System.Web.HttpContext.Current.Session["CurrentTechnologyId"] : 0;
            var currentProjType = System.Web.HttpContext.Current.Session["CurrentProjectType"] is int ? (int)System.Web.HttpContext.Current.Session["CurrentProjectType"] : 0;

            if (currentTech == 0 || currentProjType == 0)
            {
                return RedirectToAction("Setup");
            }

            var steps = Mapper.Map<List<Step>, List<StepModel>>(_manager.GetSteps(currentTech, currentProjType));

            return View(steps);
        }

        // GET: Step/Create
        public ActionResult Create()
        {
            var currentTech = System.Web.HttpContext.Current.Session["CurrentTechnologyId"] is int ? (int)System.Web.HttpContext.Current.Session["CurrentTechnologyId"] : 0;
            var currentProjType = System.Web.HttpContext.Current.Session["CurrentProjectType"] is int ? (int)System.Web.HttpContext.Current.Session["CurrentProjectType"] : 0;

            ViewBag.Technology = new SelectList(_manager.Db.Technologies, "Id", "TechnologyName", currentTech);
            ViewBag.EngineeringLevel = new SelectList(_manager.Db.Engineers, "Id", "EngineeringLevel");
            ViewBag.ProjectType = new SelectList(_manager.GetProjectTypes(), "Id", "ProjectType1", currentProjType);

            return View();
        }

        // POST: Step/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,StepDescription,EngineeringLevel,Hours,Technology,ProjectType")] StepModel stepModel)
        {
            if (ModelState.IsValid)
            {
                var model = Mapper.Map<StepModel, Step>(stepModel);

                var inserted = _manager.InsertStep(model);

                if (inserted)
                {
                    return RedirectToAction("Index");
                }
            }

            ViewBag.Technology = new SelectList(_manager.Db.Technologies, "Id", "TechnologyName", stepModel.Technology);
            ViewBag.EngineeringLevel = new SelectList(_manager.Db.Engineers, "Id", "EngineeringLevel", stepModel.EngineeringLevel);
            ViewBag.ProjectType = new SelectList(_manager.GetProjectTypes(), "Id", "ProjectType1", stepModel.ProjectType);

            return View(stepModel);
        }

        // GET: Step/Edit/5
        public ActionResult Edit(int id)
        {
            var step = Mapper.Map<Step, StepModel>(_manager.GetStepById(id));
            if (step == null)
            {
                return HttpNotFound();
            }
            ViewBag.Technology = new SelectList(_manager.Db.Technologies, "Id", "TechnologyName", step.Technology);
            ViewBag.EngineeringLevel = new SelectList(_manager.Db.Engineers, "Id", "EngineeringLevel", step.EngineeringLevel);
            ViewBag.ProjectType = new SelectList(_manager.GetProjectTypes(), "Id", "ProjectType1", step.ProjectType);

            return View(step);
        }

        // POST: Step/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,StepDescription,EngineeringLevel,Hours,Technology,ProjectType")] StepModel stepModel)
        {
            if (ModelState.IsValid)
            {
                var model = Mapper.Map<StepModel, Step>(stepModel);

                var updated = _manager.UpdateStep(model);
                if (updated)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.Technology = new SelectList(_manager.Db.Technologies, "Id", "TechnologyName", stepModel.Technology);
            ViewBag.EngineeringLevel = new SelectList(_manager.Db.Engineers, "Id", "EngineeringLevel", stepModel.EngineeringLevel);
            ViewBag.ProjectType = new SelectList(_manager.GetProjectTypes(), "Id", "ProjectType1",stepModel.ProjectType);

            return View(stepModel);
        }

        // GET: Step/Delete/5
        public ActionResult Delete(int id)
        {
            var step = Mapper.Map<Step, StepModel>(_manager.GetStepById(id));
            if (step == null)
            {
                return HttpNotFound();
            }
            return View(step);
        }

        // POST: Step/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (_manager.RemoveStep(id))
            {
                return RedirectToAction("Index");
            }
            return View(id);
        }

        public void UpdateStepNumber(int id, int fromPosition, int toPosition, string direction, int tech, int projType)
        {
            
            if (direction == "back")
            {
                var movedSteps = _manager.Db.Steps.Where(t => t.Technology == tech && t.ProjectType == projType).Where(c => (toPosition <= c.StepNumber && c.StepNumber <= fromPosition)).ToList();

                foreach (var step in movedSteps)
                {
                    step.StepNumber++;
                        
                }
            }
            else
            {
                var movedSteps = _manager.Db.Steps.Where(t => t.Technology == tech && t.ProjectType == projType).Where(c => (fromPosition <= c.StepNumber && c.StepNumber <= toPosition)).ToList();
                foreach (var step in movedSteps)
                {
                    step.StepNumber--;
                        
                }
            }

            _manager.Db.Steps.First(c => c.Id == id).StepNumber = toPosition;
            _manager.Db.SaveChanges();

        }
    }
}
