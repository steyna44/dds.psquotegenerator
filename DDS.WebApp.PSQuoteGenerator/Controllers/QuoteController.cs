﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;
using DDS.Core.BusinessLayer.Methods.QuoteManagement;
using DDS.Core.BusinessObjects.Quote;
using DDS.Core.BusinessObjects.Technology;
using DDS.Core.DataLayer;
using DDS.WebApp.PSQuoteGenerator.Models;
using Rotativa;
using Rotativa.Options;
using WebGrease.Css.Ast.Selectors;

namespace DDS.WebApp.PSQuoteGenerator.Controllers
{
    public class QuoteController : Controller
    {

        private readonly QuoteManagement _manager = new QuoteManagement();

        // GET: Quote
        public ActionResult Index()
        {
            var model = Mapper.Map<List<Quote>, List<QuoteModel>>(_manager.GetQuotes());
            return View(model);
        }

        public FileResult Download(string file)
        {

            try
            {
                var u = new Uri(file);
                var fileName = string.Empty;

                if (u.IsFile)
                    fileName = Path.GetFileName(u.AbsolutePath);

                var fileBytes = System.IO.File.ReadAllBytes(file);
                var response = new FileContentResult(fileBytes, "application/octet-stream")
                {
                    FileDownloadName = fileName
                };
                return response;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // GET: Quote/Create
        public ActionResult Create()
        {
            ViewBag.Client = new SelectList(_manager.GetClients(), "Id", "CombinedName");
            return View();
        }

        // POST: Quote/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,QuoteName,QuoteDesc,Client,QuoteRefNumber")] QuoteModel quoteModel)
        {
            ViewBag.Client = new SelectList(_manager.GetClients(), "Id", "CombinedName", quoteModel.Client);
            
            if (ModelState.IsValid)
            {
                var proposalFile = Request.Files["ProposalPath"];
                if (proposalFile != null && proposalFile.ContentLength > 0)
                {
                    const string path = @"C:\QuoteProposals\";

                    if (proposalFile.ContentLength > 4194304)
                    {
                        ModelState.AddModelError("ProposalPath", "The size of the file should not exceed 4 MB");
                        return View(quoteModel);
                    }

                    var supportedTypes = new[] { "doc", "docx" };

                    var extension = Path.GetExtension(proposalFile.FileName);
                    if (extension != null)
                    {
                        var fileExt = extension.Substring(1);

                        if (!supportedTypes.Contains(fileExt))
                        {
                            ModelState.AddModelError("ProposalPath", "Invalid type. Only the following types (doc, docx) are supported.");
                            return View(quoteModel);
                        }

                        var cleanRef = quoteModel.QuoteRefNumber.Replace(":", string.Empty).Replace("\\", string.Empty).Replace("/", string.Empty);
                        var newFileName = "Proposal-" + cleanRef + " " + DateTime.Now.ToString("yyyyMMddhhmmss") + "." + fileExt;
                        var proposalPath = path + newFileName;
                        proposalFile.SaveAs(proposalPath);

                        quoteModel.ProposalPath = proposalPath;
                    }

                    
                }

                var model = Mapper.Map<QuoteModel, Quote>(quoteModel);

                var inserted = _manager.InsertQuote(model);

                if (inserted == 0)
                {
                    RedirectToAction("Index");
                }

                return RedirectToAction("TechnologySelection", new { id = inserted });
            }

            
            return View(quoteModel);
        }

        // GET: Quote/TechnologySelection/5
        public ActionResult TechnologySelection(int id)
        {
            ViewBag.SelectedTechnologies = new MultiSelectList(_manager.GetTechnologies(), "Id", "TechnologyName");
            
            return View();
        }

        // POST: Quote/TechnologySelection/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TechnologySelection([Bind(Include = "Id,SelectedTechnologies")] QuoteTechnologyModel technologies)
        {
            ViewBag.SelectedTechnologies = new MultiSelectList(_manager.GetTechnologies(), "Id", "TechnologyName", technologies.SelectedTechnologies);
            if (technologies.SelectedTechnologies == null)
            {
                ModelState.AddModelError("SelectedTechnologies", "Please select atleast one Technology");
                return View();
            }

            if (technologies.SelectedTechnologies.Count > 0)
            {
                TempData["complexityModel"] = _manager.GetComplexities(technologies.SelectedTechnologies);

                var savedQuoted = _manager._db.Quotes.Find(technologies.Id);
                savedQuoted.PhysicalDevices = technologies.SelectedTechnologies.Count;
                _manager._db.SaveChanges();

                return RedirectToAction("TechnologyComplexity", new {id = technologies.Id});
            }

            return View();
        }

        // GET: Quote/TechnologyComplexity/5
        public ActionResult TechnologyComplexity(int id)
        {
            var complexities = (List<ComplexityObj>)TempData["complexityModel"];
            var model = Mapper.Map<List<ComplexityObj>, List<QuoteTechnologyComplexity>>(complexities);
            return View(model);
        }

        // POST: Quote/TechnologyComplexity/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TechnologyComplexity([Bind(Include = "Id,Options")] QuoteTechnologyComplexity complexities)
        {
            var manualIds = new List<int>();
            var complexStepsFound = false;
            var projectTypeTechnologyCombo = new List<QuoteCombo>();
            var nacTechologies = new List<AdditionalDataModel>();

            foreach (var option in complexities.Options)
            {
                var currentTechnology = int.Parse(option.Substring(0, option.IndexOf('-')));
                var currentProjectType = int.Parse(option.Substring(option.IndexOf('-') + 1));
                var currentCombo = new QuoteCombo()
                {
                    CurrentProjectType = currentProjectType,
                    CurrentTechnology = currentTechnology
                };
                projectTypeTechnologyCombo.Add(currentCombo);

                if (currentProjectType != 555)
                {
                    _manager.InsertLineItems(currentTechnology, currentProjectType, complexities.Id);

                    //check for complex steps
                    var complexSteps =_manager._db.ComplexSteps.Where(cs => cs.Technology == currentTechnology && cs.ProjectType == currentProjectType).ToList();

                    if (complexSteps.Count > 0)
                    {
                        complexStepsFound = true;
                    }

                }
                else if (currentProjectType == 555)
                {
                    manualIds.Add(currentTechnology);
                }

                //check for Standard NAC technologies to determine wheter or not to ask about Nodes,VLANs and SSIDs
                //check technology category
                var technologies =_manager._db.Technologies.Find(currentTechnology);

                if (technologies.TechnologyCategory != 3 && currentProjectType != 3) continue;

                var nacTech = new AdditionalDataModel()
                {
                    TechnologyId = technologies.Id,
                    QuoteId = complexities.Id,
                    Technology = technologies
                };
                nacTechologies.Add(nacTech);
            }

            if (nacTechologies.Count > 0)
            {
                if (manualIds.Count > 0)
                {
                    System.Web.HttpContext.Current.Session["ManualIds"] = manualIds;
                }

                if (complexStepsFound)
                {
                    System.Web.HttpContext.Current.Session["quoteCombo"] = projectTypeTechnologyCombo;
                }

                System.Web.HttpContext.Current.Session["NACTechnologies"] = nacTechologies;
                return RedirectToAction("ProcessNacVariables", new { id = complexities.Id });
            }

            if (complexStepsFound)
            {
                //TempData["quoteCombo"] = projectTypeTechnologyCombo;
                System.Web.HttpContext.Current.Session["quoteCombo"] = projectTypeTechnologyCombo;

                if (manualIds.Count > 0)
                {
                    System.Web.HttpContext.Current.Session["ManualIds"] = manualIds;
                }

                return RedirectToAction("ProcessComplexSteps", new { id = complexities.Id});
            }

            if (manualIds.Count > 0)
            {
                System.Web.HttpContext.Current.Session["ManualIds"] = manualIds;
                return RedirectToAction("ProcessManualSteps", new {id = complexities.Id});
            }

            _manager._db.Quotes.Find(complexities.Id).Status = "STEPS SELECTED";
            _manager._db.SaveChanges();

            return RedirectToAction("ProcessAdditional", new { id = complexities.Id });
        }

        // GET: Quote/ProcessNacVariables/5
        public ActionResult ProcessNacVariables(int id)
        {

            var nacTechnologyModel = System.Web.HttpContext.Current.Session["NACTechnologies"] is List<AdditionalDataModel> ? (List<AdditionalDataModel>)System.Web.HttpContext.Current.Session["NACTechnologies"] : new List<AdditionalDataModel>();

            if (nacTechnologyModel.Count == 0) return RedirectToAction("ContinueQuote", new { id });

            return View(nacTechnologyModel);
        }

        // POST: Quote/ProcessNacVariables/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessNacVariables(List<AdditionalDataModel> nacTechnologiesModel)
        {
            var nacTechnology = nacTechnologiesModel.FirstOrDefault();
            if (nacTechnology != null)
            {
                var currentQuoteId = nacTechnology.QuoteId;

                foreach (var technology in nacTechnologiesModel)
                {
                    var additionalData = new AdditionalData()
                    {
                        Nodes = technology.Nodes,
                        VLANs = technology.VLANs,
                        TechnologyId = technology.TechnologyId,
                        QuoteId = technology.QuoteId,
                        SSIDs = technology.SSIDs
                    };
                    _manager._db.AdditionalDatas.Add(additionalData);
                    _manager._db.SaveChanges();
                }

                var quoteCombo = System.Web.HttpContext.Current.Session["quoteCombo"] is List<QuoteCombo> ? (List<QuoteCombo>)System.Web.HttpContext.Current.Session["quoteCombo"] : new List<QuoteCombo>();
                if (quoteCombo.Count > 0)
                {
                    return RedirectToAction("ProcessComplexSteps", new { id = currentQuoteId });
                }
                var manualIds = System.Web.HttpContext.Current.Session["ManualIds"] is List<int> ? (List<int>)System.Web.HttpContext.Current.Session["ManualIds"] : new List<int>();
                if (manualIds.Count > 0)
                {
                    return RedirectToAction("ProcessManualSteps", new { id = currentQuoteId });
                }

                _manager._db.Quotes.Find(currentQuoteId).Status = "STEPS SELECTED";
                _manager._db.SaveChanges();

                return RedirectToAction("ProcessAdditional", new { id = currentQuoteId });
            }

            return RedirectToAction("Create");
        }

        // GET: Quote/ProcessComplexSteps/5
        public ActionResult ProcessComplexSteps(int id)
        {
            var complexStepModel = new List<ComplexStepContinued>();

            //var quoteCombo = TempData["quoteCombo"] as List<QuoteCombo>;
            var quoteCombo = System.Web.HttpContext.Current.Session["quoteCombo"] is List<QuoteCombo> ? (List<QuoteCombo>)System.Web.HttpContext.Current.Session["quoteCombo"] : new List<QuoteCombo>();

            if (quoteCombo.Count == 0) return RedirectToAction("ContinueQuote", new { id });
            foreach (var combo in quoteCombo)
            {
                //find complex steps
                var combo1 = combo;
                var steps = _manager._db.ComplexSteps.Where(cs => cs.ProjectType == combo1.CurrentProjectType && cs.Technology == combo1.CurrentTechnology).ToList();
                foreach (var step in steps)
                {
                    var complexStepContinued = new ComplexStepContinued()
                    {
                        TechName = step.Technology1.TechnologyName,
                        StepCategory = step.StepType,
                        StepDescription = step.StepDescription,
                        ComplexStepId = step.Id,
                        QuoteId = id,
                        EngineerLevel = step.Engineer.EngineeringLevel,
                        ProjectType = step.ProjectType1.ProjectType1
                    };
                    complexStepModel.Add(complexStepContinued);
                }
            }

            return View(complexStepModel);
        }

        // POST: Quote/ProcessComplexSteps/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessComplexSteps(List<ComplexStepContinued> complexSteps)
        {
            var complexStepContinued = complexSteps.FirstOrDefault();
            if (complexStepContinued != null)
            {
                var currentQuoteId = complexStepContinued.QuoteId;


                foreach (var step in complexSteps)
                {
                    
                    if (step.Include == false)
                    {
                        continue;
                    }

                    var value1 = 1;
                    var value2 = 1;

                    if (step.Value1 != 0)
                    {
                        value1 = step.Value1;
                    }

                    if (step.Value2 != 0)
                    {
                        value2 = step.Value2;
                    }

                    

                    var complexLineItem = new ComplexLineItem()
                    {
                        QuoteId = step.QuoteId,
                        ComplexStepId = step.ComplexStepId,
                        Value1 = value1,
                        Value2 = value2
                    };

                    _manager._db.ComplexLineItems.Add(complexLineItem);
                    _manager._db.SaveChanges();

                }

                var manualIds = System.Web.HttpContext.Current.Session["ManualIds"] is List<int> ? (List<int>)System.Web.HttpContext.Current.Session["ManualIds"] : new List<int>();
                if (manualIds.Count > 0)
                {
                    return RedirectToAction("ProcessManualSteps", new { id = currentQuoteId });
                }

                _manager._db.Quotes.Find(currentQuoteId).Status = "STEPS SELECTED";
                _manager._db.SaveChanges();

                return RedirectToAction("ProcessAdditional", new { id = currentQuoteId });
            }

            return RedirectToAction("Create");
        }

        // GET: Quote/ProcessManualSteps/5
        public ActionResult ProcessManualSteps(int id)
        {
            var manualIds = System.Web.HttpContext.Current.Session["ManualIds"] is List<int> ? (List<int>)System.Web.HttpContext.Current.Session["ManualIds"] : new List<int>();

            if (manualIds.Count>0)
            {
                var manualSteps = new List<ManualStepContinuedModel>();

                foreach (var manualId in manualIds)
                {
                    var techCategory = _manager._db.Technologies.Where(t=>t.Id == manualId).Select(t=>t.TechnologyCategory).FirstOrDefault();
                    var techName = _manager._db.Technologies.Where(t => t.Id == manualId).Select(t => t.TechnologyName).FirstOrDefault();
                    var steps = _manager._db.ManualSteps.Where(s => s.StepCategory == techCategory).ToList();

                    foreach (var step in steps)
                    {
                        var manualStep = new ManualStepContinuedModel()
                        {
                            TechName = techName,
                            StepCategory = techCategory,
                            StepDescription = step.StepDescription,
                            ManualStepId = step.Id,
                            QuoteId = id
                        };

                        manualSteps.Add(manualStep);
                    }
                
                }


                ViewData["EngineerLevel"] = _manager._db.Engineers.ToList();

                return View(manualSteps);
            }

            return RedirectToAction("ContinueQuote", new {id});
        }

        // POST: Quote/ProcessManualSteps/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessManualSteps( List<ManualStepContinuedModel> manualSteps)
        {
            foreach (var step in manualSteps)
            {
                if (string.IsNullOrEmpty(step.Hours) || step.Include == false)
                {
                    continue;
                }

                var hours = Int32.Parse(step.Hours);

                var manualLineItem = new ManualLineItem()
                {
                    QuoteId = step.QuoteId,
                    ManualStepId = step.ManualStepId,
                    Hours = hours,
                    EngineerLevel = step.EngineerLevel
                };

                _manager._db.ManualLineItems.Add(manualLineItem);
                _manager._db.SaveChanges();
                
            }
            _manager._db.Quotes.Find(manualSteps.FirstOrDefault().QuoteId).Status = "STEPS SELECTED";
            _manager._db.SaveChanges();
            return RedirectToAction("ProcessAdditional", new {id = manualSteps.FirstOrDefault().QuoteId});
        }

        // GET: Quote/ProcessAdditional/5
        public ActionResult ProcessAdditional(int id)
        {
            var pmDeliverables = _manager._db.PMDeliverables.ToList();
            ViewData["ManagerLevel"] = _manager._db.Managers.ToList();
            ViewData["QuoteId"] = id;
            return View(pmDeliverables);
        }

        // GET: Quote/ProcessAdditional/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessAdditional(List<AdditionalLineItemsModel> additionalLineItems)
        {
            foreach (var additionalLineItem in additionalLineItems)
            {
                if (string.IsNullOrEmpty(additionalLineItem.Hours) || additionalLineItem.Include == false)
                {
                    continue;
                }
                var hours = Int32.Parse(additionalLineItem.Hours);
                var additionalItem = new AdditionalLineItem()
                {
                    Hours = hours,
                    ManagerLevel = additionalLineItem.ManagerLevel,
                    PMDeliverableId = additionalLineItem.PMDeliverableId,
                    QuoteId = additionalLineItem.QuoteId
                };

                _manager._db.AdditionalLineItems.Add(additionalItem);
                _manager._db.SaveChanges();
                
            }
            _manager._db.Quotes.Find(additionalLineItems.FirstOrDefault().QuoteId).Status = "PM DELIVERABLES SELECTED";
            _manager._db.SaveChanges();
            return RedirectToAction("ProcessSundry", new {id = additionalLineItems.FirstOrDefault().QuoteId});
        }

        // GET: Quote/ProcessSundry/5
        public ActionResult ProcessSundry(int id)
        {
            var sundryCosts = _manager._db.SundryCosts.ToList();
            ViewData["QuoteId"] = id;
            return View(sundryCosts);
        }

        // GET: Quote/ProcessSundry/5
        [HttpPost]
        public ActionResult ProcessSundry(List<SundryLineItemsModel> sundryLineItemsModel)
        {
            foreach (var sundryLineItems in sundryLineItemsModel)
            {
                if (string.IsNullOrEmpty(sundryLineItems.Quantity) || sundryLineItems.Include == false)
                {
                    continue;
                }
                var qty = Int32.Parse(sundryLineItems.Quantity);
                var sundryItem = new SundryLineItem()
                {
                    QuoteId = sundryLineItems.QuoteId,
                    SundryCostId = sundryLineItems.SundryCostId,
                    Quantity = qty
                };
                _manager._db.SundryLineItems.Add(sundryItem);
                _manager._db.SaveChanges();
            }
            _manager._db.Quotes.Find(sundryLineItemsModel.FirstOrDefault().QuoteId).Status = "SUNDRY ITEMS SELECTED";
            _manager._db.SaveChanges();
            //return RedirectToAction("Index");
            return RedirectToAction("GenerateQuote", new { id = sundryLineItemsModel.FirstOrDefault().QuoteId });
        }

        //GET: Quote/GenerateQuote/5
        public ActionResult GenerateQuote(int id)
        {
            var model = new QuoteManagement().GetQuoteItems(id);
            var quote =_manager._db.Quotes.Find(id);
            quote.Status = "Completed";
            _manager._db.SaveChanges();
            return View(model);
        }

        public ActionResult GeneratePDF(int id)
        {
            var manager = new QuoteManagement().GetQuoteItems(id);

            var fileName = "Quote_Summary_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";

            return new ViewAsPdf("QuoteDownload", manager)
            {
                FileName = fileName,
                PageSize = Size.A4,
                PageOrientation = Orientation.Portrait,
                PageMargins = { Left = 5, Right = 0 }
            };
        }

        // GET: Quote/GenerateQuote/5
        //public ActionResult GenerateItemized(int id)
        //{
        //    var model = new QuoteManagement().GetQuoteItems(id);

        //    return View(model);
        //}

        public ActionResult DownloadItemized(int id)
        {
            var manager = new QuoteManagement().GetQuoteItems(id);

            var fileName = "Quote_Itemised_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";

            return new ViewAsPdf("DownloadItemized", manager)
            {
                FileName = fileName,
                PageSize = Size.A4,
                PageOrientation = Orientation.Portrait,
                PageMargins = { Left = 5, Right = 0 }
            };
        }

        // GET: Quote/Edit/5
        public ActionResult Edit(int id)
        {

            var manager = _manager.GetQuoteById(id);
            var quoteModel = Mapper.Map<Quote,QuoteModel>(manager);
            ViewBag.Client = new SelectList(_manager.GetClients(), "Id", "Company", quoteModel.Client);

            return View(quoteModel);
        }

        // POST: Quote/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,QuoteName,QuoteDesc,ProposalPath,Status,Client,QuoteDate,QuoteRefNumber,ProjectType")] QuoteModel quoteModel)
        {
            var savedQuote = _manager.GetQuoteById(quoteModel.Id);
            quoteModel.QuoteDate = savedQuote.QuoteDate;
            quoteModel.QuoteRefNumber = savedQuote.QuoteRefNumber;
            quoteModel.Status = savedQuote.Status;

            if (ModelState.IsValid)
            {
                

                var proposalFile = Request.Files["ProposalPath"];
                if (proposalFile != null && proposalFile.ContentLength > 0)
                {
                    const string path = @"C:\QuoteProposals\";

                    if (proposalFile.ContentLength > 4194304)
                    {
                        ModelState.AddModelError("ProposalPath", "The size of the file should not exceed 4 MB");
                        return View(quoteModel);
                    }

                    var supportedTypes = new[] { "doc", "docx" };

                    var extension = Path.GetExtension(proposalFile.FileName);
                    if (extension != null)
                    {
                        var fileExt = extension.Substring(1);

                        if (!supportedTypes.Contains(fileExt))
                        {
                            ModelState.AddModelError("ProposalPath", "Invalid type. Only the following types (doc, docx) are supported.");
                            return View(quoteModel);
                        }

                        var cleanRef = quoteModel.QuoteRefNumber.Replace(":", string.Empty).Replace("\\", string.Empty).Replace("/", string.Empty);
                        var newFileName = "Proposal-" + cleanRef + " " + DateTime.Now.ToString("yyyyMMddhhmmss") + "." + fileExt;
                        var proposalPath = path + newFileName;
                        proposalFile.SaveAs(proposalPath);

                        quoteModel.ProposalPath = proposalPath;
                    }


                }
                var model = Mapper.Map<QuoteModel, Quote>(quoteModel);
                var updated = _manager.UpdateQuote(model);
                if (updated)
                {
                    return RedirectToAction("Index");
                }
                
            }
            ViewBag.Client = new SelectList(_manager.GetClients(), "Id", "CombinedName", quoteModel.Client);
            return View(quoteModel);
        }

        public ActionResult ContinueQuote(int id)
        {

            var manager = _manager.GetQuoteById(id);
            var quoteModel = Mapper.Map<Quote,QuoteModel>(manager);
            ViewBag.Client = new SelectList(_manager.GetClients(), "Id", "CombinedName", quoteModel.Client);

            return View(quoteModel);
        }

        // POST: Quote/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ContinueQuote([Bind(Include = "Id,QuoteName,QuoteDesc,ProposalPath,Status,Client,QuoteDate,QuoteRefNumber,ProjectType")] QuoteModel quoteModel)
        {
            var savedQuote = _manager.GetQuoteById(quoteModel.Id);
            quoteModel.QuoteDate = savedQuote.QuoteDate;
            quoteModel.QuoteRefNumber = savedQuote.QuoteRefNumber;
            quoteModel.Status = savedQuote.Status;

            if (ModelState.IsValid)
            {
                

                var proposalFile = Request.Files["ProposalPath"];
                if (proposalFile != null && proposalFile.ContentLength > 0)
                {
                    const string path = @"C:\QuoteProposals\";

                    if (proposalFile.ContentLength > 4194304)
                    {
                        ModelState.AddModelError("ProposalPath", "The size of the file should not exceed 4 MB");
                        return View(quoteModel);
                    }

                    var supportedTypes = new[] { "doc", "docx" };

                    var extension = Path.GetExtension(proposalFile.FileName);
                    if (extension != null)
                    {
                        var fileExt = extension.Substring(1);

                        if (!supportedTypes.Contains(fileExt))
                        {
                            ModelState.AddModelError("ProposalPath", "Invalid type. Only the following types (doc, docx) are supported.");
                            return View(quoteModel);
                        }

                        var cleanRef = quoteModel.QuoteRefNumber.Replace(":", string.Empty).Replace("\\", string.Empty).Replace("/", string.Empty);
                        var newFileName = "Proposal-" + cleanRef + " " + DateTime.Now.ToString("yyyyMMddhhmmss") + "." + fileExt;
                        var proposalPath = path + newFileName;
                        proposalFile.SaveAs(proposalPath);

                        quoteModel.ProposalPath = proposalPath;
                    }


                }
                var model = Mapper.Map<QuoteModel, Quote>(quoteModel);
                var updated = _manager.UpdateQuote(model);

                var cleanedTables = _manager.CleanTables(quoteModel.Id);

                if (updated && cleanedTables)
                {
                    return RedirectToAction("TechnologySelection", new { id = quoteModel.Id });
                }
                else
                {
                    return RedirectToAction("Index");
                }
                
            }
            ViewBag.Client = new SelectList(_manager.GetClients(), "Id", "CombinedName", quoteModel.Client);
            return View(quoteModel);
        }

        // GET: Quote/Delete/5
        public ActionResult Delete(int id)
        {

            var quoteModel = _manager._db.Quotes.Find(id);
            if (quoteModel == null)
            {
                return HttpNotFound();
            }
            return View(quoteModel);
        }

        // POST: Quote/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var quoteModel = _manager._db.Quotes.Find(id);
            _manager._db.Quotes.Remove(quoteModel);
            _manager._db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}
