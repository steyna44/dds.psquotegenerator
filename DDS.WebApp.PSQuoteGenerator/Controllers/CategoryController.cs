﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DDS.Core.BusinessLayer.Methods.ProjectConfiguration;
using DDS.Core.DataLayer;
using DDS.WebApp.PSQuoteGenerator.Models;

namespace DDS.WebApp.PSQuoteGenerator.Controllers
{
    public class CategoryController : Controller
    {
        private readonly CategoryConfiguration _manager = new CategoryConfiguration();

        // GET: Category
        public ActionResult Index()
        {
            var model = Mapper.Map<List<Category>, List<CategoryModel>>(_manager.GetCategories());

            return View(model);
        }


        // GET: Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Category/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CategoryName")] CategoryModel categoryModel)
        {
            if (!ModelState.IsValid) return View(categoryModel);

            var model = Mapper.Map<CategoryModel, Category>(categoryModel);

            var inserted = _manager.InsertCategory(model);

            if (inserted)
            {
                return RedirectToAction("Index");
            }

            return View(categoryModel);
        }

        // GET: Category/Edit/5
        public ActionResult Edit(int id)
        {
            var model = Mapper.Map<Category, CategoryModel>(_manager.GetCategoryById(id));

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Category/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CategoryName")] CategoryModel categoryModel)
        {
            if (!ModelState.IsValid) return View(categoryModel);
            var model = Mapper.Map<CategoryModel, Category>(categoryModel);

            var updated = _manager.UpdateCategory(model);
            if (updated)
            {
                return RedirectToAction("Index");
            }

            return View(categoryModel);
        }

        // GET: Category/Delete/5
        public ActionResult Delete(int id)
        {
            var model = Mapper.Map<Category, CategoryModel>(_manager.GetCategoryById(id));

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (_manager.RemoveCategory(id))
            {
                return RedirectToAction("Index");
            }
            return View(id);
        }
    }
}
