﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DDS.Core.BusinessLayer.Methods.ProjectConfiguration;
using DDS.Core.DataLayer;
using DDS.WebApp.PSQuoteGenerator.Models;

namespace DDS.WebApp.PSQuoteGenerator.Controllers
{
    public class SundryCostController : Controller
    {
        private readonly SundryCostConfiguration _manager = new SundryCostConfiguration();

        // GET: SundryCostModels
        public ActionResult Index()
        {
            var model = Mapper.Map<List<SundryCost>, List<SundryCostModel>>(_manager.GetSundryCosts());

            return View(model);
        }

        // GET: SundryCostModels/Create
        public ActionResult Create()
        {
            var type1 = new { id = "Travel, Accom & SNT", Name = "Travel, Accom & SNT" };
            var type2 = new { id = "Other", Name = "Other" };

            var typeList = new List<Object> { type1, type2 };

            ViewBag.Type = new SelectList(typeList, "Id", "Name");
            return View();
        }

        // POST: SundryCostModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Description,Type,Rate")] SundryCostModel sundryCostModel)
        {
            if (!ModelState.IsValid) return View(sundryCostModel);

            var model = Mapper.Map<SundryCostModel, SundryCost>(sundryCostModel);

            var inserted = _manager.InsertSundryCost(model);

            if (inserted)
            {
                return RedirectToAction("Index");
            }
            var type1 = new { id = "Travel, Accom & SNT", Name = "Travel, Accom & SNT" };
            var type2 = new { id = "Other", Name = "Other" };

            var typeList = new List<Object> { type1, type2 };

            ViewBag.Type = new SelectList(typeList, "Id", "Name",sundryCostModel.Type);
            return View(sundryCostModel);
        }

        // GET: SundryCostModels/Edit/5
        public ActionResult Edit(int id)
        {
            var model = Mapper.Map<SundryCost, SundryCostModel>(_manager.GetSundryCostById(id));

            if (model == null)
            {
                return HttpNotFound();
            }
            var type1 = new { id = "Travel, Accom & SNT", Name = "Travel, Accom & SNT" };
            var type2 = new { id = "Other", Name = "Other" };

            var typeList = new List<Object> { type1, type2 };

            ViewBag.Type = new SelectList(typeList, "Id", "Name", model.Type);
            return View(model);
        }

        // POST: SundryCostModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Description,Type,Rate")] SundryCostModel sundryCostModel)
        {
            if (!ModelState.IsValid) return View(sundryCostModel);
            var model = Mapper.Map<SundryCostModel, SundryCost>(sundryCostModel);

            var updated = _manager.UpdateSundryCost(model);
            if (updated)
            {
                return RedirectToAction("Index");
            }
            var type1 = new { id = "Travel, Accom & SNT", Name = "Travel, Accom & SNT" };
            var type2 = new { id = "Other", Name = "Other" };

            var typeList = new List<Object> { type1, type2 };

            ViewBag.Type = new SelectList(typeList, "Id", "Name", sundryCostModel.Type);
            return View(sundryCostModel);
        }

        // GET: SundryCostModels/Delete/5
        public ActionResult Delete(int id)
        {
            var model = Mapper.Map<SundryCost, SundryCostModel>(_manager.GetSundryCostById(id));

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: SundryCostModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (_manager.RemoveSundryCost(id))
            {
                return RedirectToAction("Index");
            }
            return View(id);
        }

    }
}
