﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DDS.Core.BusinessLayer.Methods.ProjectConfiguration;
using DDS.Core.DataLayer;
using DDS.WebApp.PSQuoteGenerator.Models;

namespace DDS.WebApp.PSQuoteGenerator.Controllers
{
    public class ManualStepController : Controller
    {
        private readonly ManualStepConfiguration _manager = new ManualStepConfiguration();

        // GET: ManualStep
        public ActionResult Index()
        {
            var steps = Mapper.Map<List<ManualStep>, List<ManualStepModel>>(_manager.GetSteps());
            return View(steps);
        }

        // GET: ManualStep/Create
        public ActionResult Create()
        {
            var categories = new CategoryConfiguration().GetCategories();

            ViewBag.StepCategory = new SelectList(categories, "Id", "CategoryName");
            return View();
        }

        // POST: ManualStep/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,StepDescription,StepCategory")] ManualStepModel manualStepModel)
        {
            if (ModelState.IsValid)
            {
                var model = Mapper.Map<ManualStepModel, ManualStep>(manualStepModel);

                var inserted = _manager.InsertStep(model);

                if (inserted)
                {
                    return RedirectToAction("Index");
                }
            }
            var categories = new CategoryConfiguration().GetCategories();

            ViewBag.StepCategory = new SelectList(categories, "Id", "CategoryName", manualStepModel.StepCategory);
            return View(manualStepModel);
        }

        // GET: ManualStep/Edit/5
        public ActionResult Edit(int id)
        {
            var step = Mapper.Map<ManualStep, ManualStepModel>(_manager.GetStepById(id));
            if (step == null)
            {
                return HttpNotFound();
            }
            var categories = new CategoryConfiguration().GetCategories();

            ViewBag.StepCategory = new SelectList(categories, "Id", "CategoryName", step.StepCategory);
            return View(step);
        }

        // POST: ManualStep/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,StepDescription,StepCategory")] ManualStepModel manualStepModel)
        {
            if (ModelState.IsValid)
            {
                var model = Mapper.Map<ManualStepModel, ManualStep>(manualStepModel);

                var updated = _manager.UpdateStep(model);
                if (updated)
                {
                    return RedirectToAction("Index");
                }
            }
            var categories = new CategoryConfiguration().GetCategories();

            ViewBag.StepCategory = new SelectList(categories, "Id", "CategoryName", manualStepModel.StepCategory);
            return View(manualStepModel);
        }

        // GET: ManualStep/Delete/5
        public ActionResult Delete(int id)
        {
            var step = Mapper.Map<ManualStep, ManualStepModel>(_manager.GetStepById(id));
            if (step == null)
            {
                return HttpNotFound();
            }
            return View(step);
        }

        // POST: ManualStep/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (_manager.RemoveStep(id))
            {
                return RedirectToAction("Index");
            }
            return View(id);
        }

        public void UpdateStepNumber(int id, int fromPosition, int toPosition, string direction)
        {

            if (direction == "back")
            {
                var movedSteps = _manager.Db.ManualSteps.Where(c => (toPosition <= c.StepNumber && c.StepNumber <= fromPosition)).ToList();

                foreach (var step in movedSteps)
                {
                    step.StepNumber++;

                }
            }
            else
            {
                var movedSteps = _manager.Db.ManualSteps.Where(c => (fromPosition <= c.StepNumber && c.StepNumber <= toPosition)).ToList();
                foreach (var step in movedSteps)
                {
                    step.StepNumber--;

                }
            }

            _manager.Db.ManualSteps.First(c => c.Id == id).StepNumber = toPosition;
            _manager.Db.SaveChanges();

        }
    }
}
