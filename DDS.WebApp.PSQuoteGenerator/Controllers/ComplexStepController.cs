﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DDS.Core.BusinessLayer.Methods.ProjectConfiguration;
using DDS.Core.DataLayer;
using DDS.WebApp.PSQuoteGenerator.Models;

namespace DDS.WebApp.PSQuoteGenerator.Controllers
{
    public class ComplexStepController : Controller
    {
        private readonly ComplexStepConfiguration _manager = new ComplexStepConfiguration();

        // GET: ComplexStep/Setup
        public ActionResult Setup()
        {
            ViewBag.Technology = new SelectList(_manager.Db.Technologies, "Id", "TechnologyName");
            ViewBag.ProjectType = new SelectList(_manager.GetProjectTypes(), "Id", "ProjectType1");

            return View();
        }

        // POST: ComplexStep/Setup
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Setup(FormCollection formCollection)
        {
            var tech = int.Parse(formCollection["Technology"]);
            System.Web.HttpContext.Current.Session["CurrentTechnologyIdComplex"] = tech;

            System.Web.HttpContext.Current.Session["CurrentTechnologyNameComplex"] = _manager.Db.Technologies.Find(tech).TechnologyName;

            var projType = int.Parse(formCollection["ProjectType"]);
            System.Web.HttpContext.Current.Session["CurrentProjectTypeComplex"] = projType;

            System.Web.HttpContext.Current.Session["currentProjTypeStringComplex"] = _manager.Db.ProjectTypes.Find(projType).ProjectType1;

            return RedirectToAction("index");
        }

        // GET: ComplexStep
        public ActionResult Index()
        {

            var currentTech = System.Web.HttpContext.Current.Session["CurrentTechnologyIdComplex"] is int ? (int)System.Web.HttpContext.Current.Session["CurrentTechnologyIdComplex"] : 0;
            var currentProjType = System.Web.HttpContext.Current.Session["CurrentProjectTypeComplex"] is int ? (int)System.Web.HttpContext.Current.Session["CurrentProjectTypeComplex"] : 0;

            if (currentTech == 0 || currentProjType == 0)
            {
                return RedirectToAction("Setup");
            }

            var steps = Mapper.Map<List<ComplexStep>, List<ComplexStepModel>>(_manager.GetComplexSteps(currentTech, currentProjType));

            return View(steps);
        }

        // GET: Step/Create
        public ActionResult Create()
        {
            var currentTech = System.Web.HttpContext.Current.Session["CurrentTechnologyIdComplex"] is int ? (int)System.Web.HttpContext.Current.Session["CurrentTechnologyIdComplex"] : 0;
            var currentProjType = System.Web.HttpContext.Current.Session["CurrentProjectTypeComplex"] is int ? (int)System.Web.HttpContext.Current.Session["CurrentProjectTypeComplex"] : 0;

            ViewBag.Technology = new SelectList(_manager.Db.Technologies, "Id", "TechnologyName", currentTech);
            ViewBag.EngineeringLevel = new SelectList(_manager.Db.Engineers, "Id", "EngineeringLevel");
            ViewBag.ProjectType = new SelectList(_manager.GetProjectTypes(), "Id", "ProjectType1", currentProjType);
            ViewBag.StepType = new SelectList(_manager.GetComplexStepTypes(), "Id", "ComplexStepType1");
            ViewBag.HourType = new SelectList(_manager.GetHourTypes(), "Id", "Description");

            return View();
        }

        // POST: ComplexStep/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,StepDescription,EngineeringLevel,Hours,Technology,ProjectType,StepType,HourType")] ComplexStepModel stepModel)
        {
            if (ModelState.IsValid)
            {
                var model = Mapper.Map<ComplexStepModel, ComplexStep>(stepModel);

                var inserted = _manager.InsertComplexStep(model);

                if (inserted)
                {
                    return RedirectToAction("Index");
                }
            }

            ViewBag.Technology = new SelectList(_manager.Db.Technologies, "Id", "TechnologyName", stepModel.Technology);
            ViewBag.EngineeringLevel = new SelectList(_manager.Db.Engineers, "Id", "EngineeringLevel", stepModel.EngineeringLevel);
            ViewBag.ProjectType = new SelectList(_manager.GetProjectTypes(), "Id", "ProjectType1", stepModel.ProjectType);
            ViewBag.StepType = new SelectList(_manager.GetComplexStepTypes(), "Id", "ComplexStepType1", stepModel.StepType);
            ViewBag.HourType = new SelectList(_manager.GetHourTypes(), "Id", "Description", stepModel.HourType);

            return View(stepModel);
        }

        // GET: ComplexStep/Edit/5
        public ActionResult Edit(int id)
        {
            var step = Mapper.Map<ComplexStep, ComplexStepModel>(_manager.GetComplexStepById(id));
            if (step == null)
            {
                return HttpNotFound();
            }
            ViewBag.Technology = new SelectList(_manager.Db.Technologies, "Id", "TechnologyName", step.Technology);
            ViewBag.EngineeringLevel = new SelectList(_manager.Db.Engineers, "Id", "EngineeringLevel", step.EngineeringLevel);
            ViewBag.ProjectType = new SelectList(_manager.GetProjectTypes(), "Id", "ProjectType1", step.ProjectType);
            ViewBag.StepType = new SelectList(_manager.GetComplexStepTypes(), "Id", "ComplexStepType1", step.StepType);
            ViewBag.HourType = new SelectList(_manager.GetHourTypes(), "Id", "Description", step.HourType);

            return View(step);
        }

        // POST: ComplexStep/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,StepDescription,EngineeringLevel,Hours,Technology,ProjectType,StepType,HourType")] ComplexStepModel stepModel)
        {
            if (ModelState.IsValid)
            {
                var model = Mapper.Map<ComplexStepModel, ComplexStep>(stepModel);

                var updated = _manager.UpdateComplexStep(model);
                if (updated)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.Technology = new SelectList(_manager.Db.Technologies, "Id", "TechnologyName", stepModel.Technology);
            ViewBag.EngineeringLevel = new SelectList(_manager.Db.Engineers, "Id", "EngineeringLevel", stepModel.EngineeringLevel);
            ViewBag.ProjectType = new SelectList(_manager.GetProjectTypes(), "Id", "ProjectType1", stepModel.ProjectType);
            ViewBag.StepType = new SelectList(_manager.GetComplexStepTypes(), "Id", "ComplexStepType1", stepModel.StepType);
            ViewBag.HourType = new SelectList(_manager.GetHourTypes(), "Id", "Description", stepModel.HourType);

            return View(stepModel);
        }

        // GET: ComplexStep/Delete/5
        public ActionResult Delete(int id)
        {
            var step = Mapper.Map<ComplexStep, ComplexStepModel>(_manager.GetComplexStepById(id));
            if (step == null)
            {
                return HttpNotFound();
            }
            return View(step);
        }

        // POST: ComplexStep/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (_manager.RemoveComplexStep(id))
            {
                return RedirectToAction("Index");
            }
            return View(id);
        }

        public void UpdateStepNumber(int id, int fromPosition, int toPosition, string direction, int tech, int projType)
        {

            if (direction == "back")
            {
                var movedSteps = _manager.Db.ComplexSteps.Where(t => t.Technology == tech && t.ProjectType == projType).Where(c => (toPosition <= c.StepNumber && c.StepNumber <= fromPosition)).ToList();

                foreach (var step in movedSteps)
                {
                    step.StepNumber++;

                }
            }
            else
            {
                var movedSteps = _manager.Db.ComplexSteps.Where(t => t.Technology == tech && t.ProjectType == projType).Where(c => (fromPosition <= c.StepNumber && c.StepNumber <= toPosition)).ToList();
                foreach (var step in movedSteps)
                {
                    step.StepNumber--;

                }
            }

            _manager.Db.ComplexSteps.First(c => c.Id == id).StepNumber = toPosition;
            _manager.Db.SaveChanges();

        }
    }
}
