﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DDS.Core.BusinessLayer.Methods
{
    public class CurrencyManagement
    {
        public static void InitializeCulture(string cultureCode)
        {
            try
            {
                if (cultureCode == null) return;
                var selectedLanguage = cultureCode;

                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(selectedLanguage); // de-DE
                Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(selectedLanguage); 
            }
            catch (Exception ex)
            {
                var test = 0;
            }
        }


    }
}
