﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

using DDS.Core.DataLayer;

namespace DDS.Core.BusinessLayer.Methods.ProjectConfiguration
{
    ////////////////////////////////////////////////////////TECHNOLOGIES////////////////////////////////////////////////////////
    public class TechnologyConfiguration
    {

        public List<Technology> GetTechnologies()
        {

            using (var db = new DataManagementContext())
            {
                return db.Technologies.Include(t=>t.Category).OrderBy(c=>c.Category.CategoryName).ToList();
            }

        } 

        public bool InsertTechnology(Technology technology)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var id = db.Technologies.Add(technology);
                    db.SaveChanges();

                    return id != null;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Technology GetTechnologyById(int id)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var technology = db.Technologies.Include(t => t.Category).Where(t => t.Id == id);
                    return technology.FirstOrDefault();
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateTechnology(Technology technology)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    db.Entry(technology).State = EntityState.Modified;

                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RemoveTechnology(int id)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var tech = db.Technologies.Find(id);
                    db.Technologies.Remove(tech);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
    ////////////////////////////////////////////////////////TECHNOLOGIES////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////CLIENTS//////////////////////////////////////////////////////////
    public class ClientConfiguration
    {
        public List<Client> GetClients()
        {
            using (var db = new DataManagementContext())
            {
                return db.Clients.ToList();
            }
        }

        public bool InsertClient(Client client)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var id = db.Clients.Add(client);
                    db.SaveChanges();

                    return id != null;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Client GetClientById(int id)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var client = db.Clients.Find(id);
                    return client;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateClient(Client client)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    db.Entry(client).State = EntityState.Modified;

                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RemoveClient(int id)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var client = db.Clients.Find(id);
                    db.Clients.Remove(client);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
    //////////////////////////////////////////////////////////CLIENTS//////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////STEPS///////////////////////////////////////////////////////////
    public class StepConfiguration
    {
        public readonly DataManagementContext Db = new DataManagementContext();

        public List<Step> GetSteps(int tech, int projType)
        {
            return Db.Steps.Where(t => t.Technology == tech && t.ProjectType == projType).Include(s => s.ProjectType1).Include(s => s.Tech).Include(s => s.Eng).ToList();
        }

        public bool InsertStep(Step step)
        {
            try
            {
                var highestStep = Db.Steps.Where(s => s.ProjectType == step.ProjectType && s.Technology == step.Technology).OrderByDescending(s=>s.StepNumber).FirstOrDefault();
                var newStepNumber = 1;
                if (highestStep != null)
                {
                    var highestStepNumber = highestStep.StepNumber;

                    newStepNumber = highestStepNumber+1;
                }
                

                step.StepNumber = newStepNumber;
                var id = Db.Steps.Add(step);
                Db.SaveChanges();

                return id != null;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Step GetStepById(int id)
        {
            try
            {
                var step = Db.Steps.Find(id);
                return step;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateStep(Step step)
        {
            try
            {

                var savedStep = Db.Steps.Find(step.Id);

                if (savedStep.ProjectType != step.ProjectType || savedStep.Technology != step.Technology)
                {
                    var highestStep = Db.Steps.Where(s => s.ProjectType == step.ProjectType && s.Technology == step.Technology).OrderByDescending(s => s.StepNumber).FirstOrDefault();
                    var newStepNumber = 1;
                    if (highestStep != null)
                    {
                        var highestStepNumber = highestStep.StepNumber;

                        newStepNumber = highestStepNumber + 1;
                    }


                    savedStep.StepNumber = newStepNumber;
                }

                
                savedStep.ProjectType = step.ProjectType;
                savedStep.StepDescription = step.StepDescription;
                savedStep.Technology = step.Technology;
                savedStep.Hours = step.Hours;
                savedStep.EngineeringLevel = step.EngineeringLevel;

                Db.SaveChanges();

                return true;
                
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RemoveStep(int id)
        {
            try
            {
                var step = Db.Steps.Find(id);
                Db.Steps.Remove(step);
                Db.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<ProjectType> GetProjectTypes()
        {
            return Db.ProjectTypes.ToList();
        }
    }
    ///////////////////////////////////////////////////////////STEPS/////////////////////////////////////////////////////////// 

    ///////////////////////////////////////////////////////////COMPLEX STEPS///////////////////////////////////////////////////////////
    public class ComplexStepConfiguration
    {
        public readonly DataManagementContext Db = new DataManagementContext();

        public List<ComplexStep> GetComplexSteps(int tech, int projType)
        {
            return Db.ComplexSteps.Where(t => t.Technology == tech && t.ProjectType == projType)
                .Include(s => s.ProjectType1)
                .Include(s => s.Technology1)
                .Include(s => s.Engineer)
                .Include(s => s.ComplexStepType)
                .Include(s => s.HourType1)
                .ToList();
        }

        public bool InsertComplexStep(ComplexStep step)
        {
            try
            {
                var highestComplexStep = Db.ComplexSteps.Where(s => s.ProjectType == step.ProjectType && s.Technology == step.Technology).OrderByDescending(s => s.StepNumber).FirstOrDefault();
                var newComplexStepNumber = 1;
                if (highestComplexStep != null)
                {
                    var highestComplexStepNumber = highestComplexStep.StepNumber;

                    newComplexStepNumber = highestComplexStepNumber + 1;
                }


                step.StepNumber = newComplexStepNumber;
                var id = Db.ComplexSteps.Add(step);
                Db.SaveChanges();

                return id != null;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public ComplexStep GetComplexStepById(int id)
        {
            try
            {
                var step = Db.ComplexSteps.Find(id);
                return step;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateComplexStep(ComplexStep step)
        {
            try
            {

                var savedComplexStep = Db.ComplexSteps.Find(step.Id);

                if (savedComplexStep.ProjectType != step.ProjectType || savedComplexStep.Technology != step.Technology)
                {
                    var highestComplexStep = Db.ComplexSteps.Where(s => s.ProjectType == step.ProjectType && s.Technology == step.Technology).OrderByDescending(s => s.StepNumber).FirstOrDefault();
                    var newComplexStepNumber = 1;
                    if (highestComplexStep != null)
                    {
                        var highestComplexStepNumber = highestComplexStep.StepNumber;

                        newComplexStepNumber = highestComplexStepNumber + 1;
                    }


                    savedComplexStep.StepNumber = newComplexStepNumber;
                }


                savedComplexStep.ProjectType = step.ProjectType;
                savedComplexStep.StepDescription = step.StepDescription;
                savedComplexStep.Technology = step.Technology;
                savedComplexStep.Hours = step.Hours;
                savedComplexStep.EngineeringLevel = step.EngineeringLevel;
                savedComplexStep.StepType = step.StepType;

                Db.SaveChanges();

                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RemoveComplexStep(int id)
        {
            try
            {
                var step = Db.ComplexSteps.Find(id);
                Db.ComplexSteps.Remove(step);
                Db.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<ProjectType> GetProjectTypes()
        {
            return Db.ProjectTypes.ToList();
        }

        public List<ComplexStepType> GetComplexStepTypes()
        {
            return Db.ComplexStepTypes.ToList();
        }

        public List<HourType> GetHourTypes()
        {
            return Db.HourTypes.ToList();
        }

    }
    ///////////////////////////////////////////////////////////COMPLEX STEPS///////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////ENGINEERS/////////////////////////////////////////////////////////
    public class EngineerConfiguration
    {
        public List<Engineer> GetEngineers()
        {
            using (var db = new DataManagementContext())
            {
                return db.Engineers.ToList();
            }
        }

        public bool InsertEngineer(Engineer engineer)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var id = db.Engineers.Add(engineer);
                    db.SaveChanges();

                    return id != null;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Engineer GetEngineerById(int id)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var engineer = db.Engineers.Find(id);
                    return engineer;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateEngineer(Engineer engineer)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    db.Entry(engineer).State = EntityState.Modified;

                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RemoveEngineer(int id)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var engineer = db.Engineers.Find(id);
                    db.Engineers.Remove(engineer);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
    /////////////////////////////////////////////////////////ENGINEERS/////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////CATEGORIES////////////////////////////////////////////////////////
    public class CategoryConfiguration
    {

        public List<Category> GetCategories()
        {

            using (var db = new DataManagementContext())
            {
                return db.Categories.ToList();
            }

        }

        public bool InsertCategory(Category category)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var id = db.Categories.Add(category);
                    db.SaveChanges();

                    return id != null;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Category GetCategoryById(int id)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var category = db.Categories.Find(id);
                    return category;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateCategory(Category category)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    db.Entry(category).State = EntityState.Modified;

                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RemoveCategory(int id)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var category = db.Categories.Find(id);
                    db.Categories.Remove(category);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
    ////////////////////////////////////////////////////////CATEGORIES////////////////////////////////////////////////////////

    //---------------------------------------------------------------------------------------------------------------------//

    ////////////////////////////////////////////////////////MANUAL STEPS//////////////////////////////////////////////////////// 
    public class ManualStepConfiguration
    {
        public readonly DataManagementContext Db = new DataManagementContext();

        public List<ManualStep> GetSteps()
        {
            return Db.ManualSteps.Include(c=>c.Category).ToList();
        }

        public bool InsertStep(ManualStep step)
        {
            try
            {
                var highestStep = Db.ManualSteps.OrderByDescending(s => s.StepNumber).FirstOrDefault();
                var newStepNumber = 1;
                if (highestStep != null)
                {
                    var highestStepNumber = highestStep.StepNumber;

                    newStepNumber = highestStepNumber + 1;
                }


                step.StepNumber = newStepNumber;
                var id = Db.ManualSteps.Add(step);
                Db.SaveChanges();

                return id != null;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public ManualStep GetStepById(int id)
        {
            try
            {
                var step = Db.ManualSteps.Include(c => c.Category).Where(s=>s.Id==id);
                return step.FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateStep(ManualStep step)
        {
            try
            {

                var savedStep = Db.ManualSteps.Find(step.Id);

                savedStep.StepDescription = step.StepDescription;

                Db.SaveChanges();

                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RemoveStep(int id)
        {
            try
            {
                var step = Db.ManualSteps.Find(id);
                Db.ManualSteps.Remove(step);
                Db.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
    ////////////////////////////////////////////////////////MANUAL STEPS//////////////////////////////////////////////////////// 

    //////////////////////////////////////////////////////////MANAGERS//////////////////////////////////////////////////////////
    public class ManagerConfiguration
    {
        public List<Manager> GetManagers()
        {
            using (var db = new DataManagementContext())
            {
                return db.Managers.ToList();
            }
        }

        public bool InsertManager(Manager manager)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var id = db.Managers.Add(manager);
                    db.SaveChanges();

                    return id != null;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Manager GetManagerById(int id)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var manager = db.Managers.Find(id);
                    return manager;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateManager(Manager manager)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    db.Entry(manager).State = EntityState.Modified;

                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RemoveManager(int id)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var manager = db.Managers.Find(id);
                    db.Managers.Remove(manager);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
    //////////////////////////////////////////////////////////MANAGERS//////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////SUNDRYCOSTS/////////////////////////////////////////////////////////
    public class SundryCostConfiguration
    {
        public List<SundryCost> GetSundryCosts()
        {
            using (var db = new DataManagementContext())
            {
                return db.SundryCosts.ToList();
            }
        }

        public bool InsertSundryCost(SundryCost sundryCost)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var id = db.SundryCosts.Add(sundryCost);
                    db.SaveChanges();

                    return id != null;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public SundryCost GetSundryCostById(int id)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var sundryCost = db.SundryCosts.Find(id);
                    return sundryCost;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateSundryCost(SundryCost sundryCost)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    db.Entry(sundryCost).State = EntityState.Modified;

                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RemoveSundryCost(int id)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var sundryCost = db.SundryCosts.Find(id);
                    db.SundryCosts.Remove(sundryCost);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
    ////////////////////////////////////////////////////////SUNDRYCOSTS/////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////PMDeliverables///////////////////////////////////////////////////////
    public class PMDeliverableConfiguration
    {
        public List<PMDeliverable> GetPMDeliverables()
        {
            using (var db = new DataManagementContext())
            {
                return db.PMDeliverables.ToList();
            }
        }

        public bool InsertPMDeliverable(PMDeliverable pmDeliverable)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var id = db.PMDeliverables.Add(pmDeliverable);
                    db.SaveChanges();

                    return id != null;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public PMDeliverable GetPMDeliverableById(int id)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var pmDeliverable = db.PMDeliverables.Find(id);
                    return pmDeliverable;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdatePMDeliverable(PMDeliverable pmDeliverable)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    db.Entry(pmDeliverable).State = EntityState.Modified;

                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RemovePMDeliverable(int id)
        {
            try
            {
                using (var db = new DataManagementContext())
                {
                    var pmDeliverable = db.PMDeliverables.Find(id);
                    db.PMDeliverables.Remove(pmDeliverable);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
    ///////////////////////////////////////////////////////PMDeliverables///////////////////////////////////////////////////////
}
