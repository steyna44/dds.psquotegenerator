﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using DDS.Core.BusinessObjects.Quote;
using DDS.Core.BusinessObjects.Technology;
using DDS.Core.DataLayer;
using iTextSharp.tool.xml.parser.state;

namespace DDS.Core.BusinessLayer.Methods.QuoteManagement
{
    public class QuoteManagement
    {
        public readonly DataManagementContext _db = new DataManagementContext();

        public List<Quote> GetQuotes()
        {
            return _db.Quotes.Include(c => c.LinkedClient).OrderBy(q => q.QuoteDate).ToList();
        }

        public int InsertQuote(Quote quote)
        {
            try
            {
                quote.QuoteDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                quote.Status = "RFQ";

                var insertQuote = _db.Quotes.Add(quote);
                _db.SaveChanges();

                return insertQuote.Id;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public Quote GetQuoteById(int quote)
        {
            try
            {
                return _db.Quotes.Find(quote);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Client> GetClients()
        {
            return _db.Clients.ToList();
        }

        public List<Technology> GetTechnologies()
        {
            //var techs = _db.Technologies.Where(t => t.Steps.Count > 0).ToList();
            var techs = _db.Technologies.ToList();

            return techs;
        }

        public void InsertLineItems(int techId, int projType, int quoteId)
        {
            try
            {
                //get steps related to current technology and project type and add them to lineitems
                var steps = _db.Steps.Where(tech => tech.Technology == techId && tech.ProjectType == projType).ToList();

                foreach (var step in steps)
                {
                    var lineItem = new LineItem()
                    {
                        QuoteId = quoteId,
                        StepId = step.Id
                    };

                    _db.LineItems.Add(lineItem);
                    _db.SaveChanges();
                }
            }
            catch (Exception)
            {
                Console.Write("failed");
            }
        }

        public List<ComplexityObj> GetComplexities(List<int> selectedTechnologies)
        {
            var complexityList = new List<ComplexityObj>();

            foreach (var selectedTechnology in selectedTechnologies)
            {
                
                var technology = selectedTechnology;
                var complexity = _db.Steps.Where(t => t.Technology == technology).Select(type => type.ProjectType).Distinct().ToList();

                var stepTypes = complexity.Select(type => type).ToList();

                var options = stepTypes.Select(stepType => _db.ProjectTypes.Find(stepType)).ToList();

                options.Add(new ProjectType(){Id=555,ProjectType1 = "Manual"});

                var item = new ComplexityObj()
                {
                    TechId = selectedTechnology,
                    TechName = _db.Technologies.Find(selectedTechnology).TechnologyName,
                    ProjectTypes = options
                };

                complexityList.Add(item);
            }

            return complexityList;
        }

        public QuoteItem GetQuoteItems(int id)
        {
            var categories = _db.Categories.Distinct().ToList();
            var quote = _db.Quotes.Find(id);
            var sundryTypes = _db.SundryCosts.Select(s => s.Type).Distinct().ToList();
            var managers = _db.Managers.Select(m => m.ManagerLevel).Distinct().ToList();
      
            //var physicalDevices = 0;

            //var categoryList = new List<string>();

            //foreach (var manualStep in _db.ManualLineItems.Where(q=>q.QuoteId == id).Select(m=>m.ManualStep.Category.CategoryName).ToList())
            //{
            //    categoryList.Add(manualStep);
            //}

            //foreach (var preconfigStep in _db.LineItems.Where(q=>q.QuoteId == id).Select(p=>p.Step.Tech.Category.CategoryName).ToList())
            //{
            //    categoryList.Add(preconfigStep);
            //}

            //if (categoryList.Count > 0)
            //{
            //    physicalDevices = categoryList.Distinct().Count();
            //}
            

            var quoteItems = new QuoteItem()
            {
                Categories = categories,
                SundryTypes = sundryTypes,
                Managers = managers,
                //PhysicalDevices = physicalDevices,
                Quote = quote
            };

            return quoteItems;
        }

        public bool UpdateQuote(Quote model)
        {
            try
            {
                var quote = _db.Quotes.Find(model.Id);
                quote.QuoteDesc = model.QuoteDesc;
                quote.Client = model.Client;
                quote.QuoteName = model.QuoteName;

                if (!string.IsNullOrEmpty(model.ProposalPath))
                {
                    quote.ProposalPath = model.ProposalPath;
                }
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

           
        }

        public bool CleanTables(int id)
        {
            try
            {
                var linkedItems = _db.LineItems.Where(q => q.QuoteId == id);
                _db.LineItems.RemoveRange(linkedItems);

                var additionalItems = _db.AdditionalLineItems.Where(q => q.QuoteId == id);
                _db.AdditionalLineItems.RemoveRange(additionalItems);

                var sundryItems = _db.SundryLineItems.Where(q => q.QuoteId == id);
                _db.SundryLineItems.RemoveRange(sundryItems);

                var manualLineItems = _db.ManualLineItems.Where(q => q.QuoteId == id);
                _db.ManualLineItems.RemoveRange(manualLineItems);

                var complexLineItems = _db.ComplexLineItems.Where(q => q.QuoteId == id);
                _db.ComplexLineItems.RemoveRange(complexLineItems);

                var additionalData = _db.AdditionalDatas.Where(q => q.QuoteId == id);
                _db.AdditionalDatas.RemoveRange(additionalData);

                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
