﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDS.Core.BusinessObjects.Quote
{
    public class QuoteCombo
    {
        public int CurrentProjectType { get; set; }
        public int CurrentTechnology { get; set; }
    }
}
