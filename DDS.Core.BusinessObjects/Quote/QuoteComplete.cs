﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDS.Core.BusinessObjects.Quote
{
    public class QuoteDetails
    {
        public int QuoteId { get; set; }

        public string ClientName { get; set; }
        public string Company { get; set; }

        public string QuoteDate { get; set; }
        public string ReferenceNumber { get; set; }

        public int TotalHoursFirewall { get; set; }
        [DataType(DataType.Currency)]
        public decimal TotalFirewall { get; set; }

        public int TotalHoursNac { get; set; }
        [DataType(DataType.Currency)]
        public decimal TotalNac { get; set; }

        public int TotalHoursWebSecurity { get; set; }
        [DataType(DataType.Currency)]
        public decimal TotalWebSecurity { get; set; }

        public int TotalPhysicalDevices { get; set; }

        public int TotalQtyOther { get; set; }
        [DataType(DataType.Currency)]
        public decimal TotalCostOther { get; set; }

        public int TotalQtyTravelEtc { get; set; }
        [DataType(DataType.Currency)]
        public decimal TotalCostTravelEtc { get; set; }

        public int PM1Hours { get; set; }
        [DataType(DataType.Currency)]
        public decimal TotalPM1 { get; set; }
        public int PM2Hours { get; set; }
        [DataType(DataType.Currency)]
        public decimal TotalPM2 { get; set; }
        public int PM3Hours { get; set; }
        [DataType(DataType.Currency)]
        public decimal TotalPM3 { get; set; }

        [DataType(DataType.Currency)]
        public decimal TotalPM { get; set; }

        [DataType(DataType.Currency)]
        public decimal TotalExVat { get; set; }
        [DataType(DataType.Currency)]
        public decimal Vat { get; set; }
        [DataType(DataType.Currency)]
        public decimal TotalIncVat { get; set; }

    }
}
