﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDS.Core.DataLayer;

namespace DDS.Core.BusinessObjects.Quote
{
    public class QuoteItem
    {
        public List<Category> Categories { get; set; }
        public List<string> SundryTypes { get; set; }
        public List<string> Managers { get; set; }
        //public int PhysicalDevices { get; set; }
        public DataLayer.Quote Quote { get; set; }
    }
}
