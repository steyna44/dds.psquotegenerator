﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDS.Core.DataLayer;

namespace DDS.Core.BusinessObjects.Technology
{
    public class ComplexityObj
    {
        //public int QuoteId { get; set; }
        public int TechId { get; set; }
        public string TechName { get; set; }
        public List<ProjectType> ProjectTypes { get; set; }
    }
}
